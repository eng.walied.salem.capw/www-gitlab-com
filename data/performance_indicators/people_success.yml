- name: Diversity - Women at GitLab
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: This is calculated as the percent of team members that identify as women through the EEOC Survey on the last day of the calendar month.
  org: People Success
  target: We aim for a target of 40%
  is_key: true
  public: true
  health:
    level: 2
    reasons:
      - Goal not met
  sisense_data:
    chart: 8677541
    dashboard: 482006
    embed: v2

- name: Diversity - Women in Management
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: This is calculated as the number of women who are people managers at GitLab, at all levels of our org structure, divided by the the total numbers of people managers at GitLab, at all levels of our org structure, on the last day of the calendar month. It is measured as the percent of women in BambooHR with a Job Grade of 8+ and have at least one direct report.
  org: People Success
  target: We aim for women to be 33% of management in Calendar Year 2022.
  is_key: true
  public: true
  health:
    level: 3
    reasons:
      - Goal met
  sisense_data:
    chart: 8676812
    dashboard: 482006
    embed: v2

- name: Diversity - Women in Senior Leadership and Executive Roles
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: This is calculated as the number of women in <a href="/company/team/structure/#senior-leaders">Senior Leadership</a> roles, which are Senior Directors and VPs, and Executive roles divided by the the total numbers of Senior Leaders and Executives on the last day of the calendar month. It is measured as the percent of women in BambooHR with a Job Grade of 11+ or CXO who are not on leave and have at least one direct report.
  org: People Success
  target: From July 2020 through December 2021, we aim for 50% of all Senior Leadership and Executive hires to be Women.
  is_key: true
  public: true
  health:
    level: 2
    reasons:
      - Below target
  sisense_data:
    chart: 9239007
    dashboard: 482006
    embed: v2

- name: New Hire Location Factor
  base_path: "/handbook/people-group/people-success-performance-indicators/"
  definition: The <a href="https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor">average
    location factor</a> of all newly hired team members within the last rolling 3
    month as of the end of the period.  (i.e If the current month is 2019-09-01 then
    the calculation pulls months 2019-06-01 to 2019-08-31). Each division and department
    has their own hire location factor target.
  target: 0.72
  org: People success
  is_key: true
  public: true
  health:
    level: 3
    reasons:
    - Currently no new hires
  sisense_data:
    chart: 6322342
    dashboard: 483663
    shared_dashboard: 11ce2c22-037a-49d3-8a0a-fcd9783070f3
    embed: v2

- name: Offer Acceptance Rate
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The number of offers accepted divided by the number of offers extended in a given month. In other words, if 50 offers were extended in June, the offer acceptance rate is the total number of those offers accepted divided by fifty. If an offer is extended June 30 but accepted July 1, it is presented in June's offer acceptance rate. This means that last month's numbers may change slightly as offers are accepted in the beginning of the next calendar month.
  target: The offer acceptance rate target is > 0.9.
  org: People Success
  is_key: true
  public: true
  health:
    level: 3
    reasons:
      - On target
  sisense_data:
    chart: 6235279
    dashboard: 482006
    embed: v2
    border: off

- name: Release Agreement Acceptance
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Release agreement acceptance rate is the percentage of team members that accept the terms and conditions of the severance agreement. We are tracking this company-wide.
  target: Greater than 90%
  org: People Success
  is_key: true
  public: true
  health:
    level: 3
    reasons:
      - Release agreement acceptance has hit target

- name: Location Factor
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Location factor directly correlates to geographical area in the Compensation Calculator. We track this by division and department to ensure we are staying true to being a global company.
  target: 0.65
  org: People Success
  is_key: true
  public: true
  health:
    level: 3
    reasons:
      - No new hires on team
  sisense_data:
    chart: 6322339
    dashboard: 483663
    embed: v2
    border: off

- name: New Hire Location Factor
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The <a href="https://about.gitlab.com/handbook/people-group/people-operations-metrics/#average-location-factor">average location factor</a> of all newly hired team members within the last rolling 3 month as of the end of the period.  (i.e If the current month is 2019-09-01 then the calculation pulls months 2019-06-01 to 2019-08-31). Each division and department has their own hire location factor target.
  target: 0.72
  org: People success
  is_key: true
  public: true
  health:
    level: 3
    reasons:
      - No new hires presently
  sisense_data:
    chart: 6322342
    dashboard: 483663
    shared_dashboard: 11ce2c22-037a-49d3-8a0a-fcd9783070f3
    embed: v2

- name: Team Member Retention (Rolling 12 Months)
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period, as well as over a calendar month. (The default period is over a rolling 12 month period.) In order to achieve the rolling 12 month team member retention target, the monthly team member total turnover target is < 1.3% (16/12).
  target: Greater than 84%
  org: People Success
  is_key: true
  public: false
  health:
    level: 3
    reasons:
      - Target met
  urls:
    - https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6251791&udv=904340

- name: Team Member Voluntary Retention (Rolling 12 Months)
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Team Member Voluntary Retention = Retention + Involuntary Turnover. GitLab measures team member voluntary retention over a rolling 12 month period, as well as over a calendar month. (The default period is over a rolling 12 month period.) In order to achieve the rolling 12 month team member voluntary retention target, the monthly metric must be >90%.
  target: Greater than 90%
  org: People Success
  is_key: true
  public: false
  health:
    level: 3
    reasons:
      - Target met
  urls:
    - https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=9592672&udv=904340

- name: Voluntary Team Member Turnover (Rolling 12 Months)
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Voluntary turnover is any instance in which a team member actively chooses to leave GitLab. GitLab measures voluntary turnover over a rolling 12 month period, as well as over a calendar month.  (The default period is over a rolling 12 month period). In order to achieve the rolling 12 month voluntary team member turnover cap, the monthly voluntary team member turnover cap is < 0.83% (10/12). Rolling Voluntary Team Member Turnover = (Number of Team Members actively choosing to leave GitLab/Average Total Team Members Count) x 100. Industry Standard Turnover is <a href="https://radford.aon.com/insights/infographics/2017/technology/q1-2017-turnover-rates-hiring-sentiment-by-industry-at-us-technology-companies">22% overall</a>:15% voluntary and 7% involuntary for software companies.
  target: Less than 10%
  org: People Success
  is_key: true
  public: false
  health:
    level: 3
    reasons:
      - Target met
  urls:
    - https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6873851&udv=904340


- name: Onboarding Satisfaction (OSAT)
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: New team member feedback of the onboarding experience in a given month. The Onboarding Satisfaction target is > 4.5. Read more about how we measure satisfaction at GitLab.
  target: Greater than 4.5
  org: People Success
  is_key: true
  public: true
  health:
    level: 2
    reasons:
      - Slightly below target
  sisense_data:
    chart: 6873595
    dashboard: 482006
    embed: v2
    border: off

- name: OSAT Buddy Experience Score
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The Buddy Experience Score is a reflection of the [buddy program experience](/handbook/people-group/general-onboarding/onboarding-buddies/) at GitLab. The Buddy Experience target score is >4.
  target: Greater than 4
  org: People Success
  is_key: false
  public: true
  health:
    level: 3
    reasons:
      - Target met
  sisense_data:
    chart: 9170079
    dashboard: 683215
    embed: v2
    border: off

- name: Discretionary bonuses
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The number of discretionary bonuses given divided by the total number of team members, in a given period as defined.
  target: Greater than 0.1
  org: People Success
  is_key: true
  public: true
  health:
    level: 2
    reasons:
        - Below target
  sisense_data:
    chart: 6251794
    dashboard: 482006
    embed: v2
    border: off

- name: Promotion Rate
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The total number of promotions excluding SDRs over a rolling 12 month period divided by the month end headcount excluding SDRs. The target promotion rate is 12% of the population with an average of a 10% OTE increase. The 12% promotion rate does not apply to SDRs who have a higher rate.  In addition, to understanding company level promotion rate, we look at promotion rates by gender.
  target: 12%
  org: People Success
  is_key: true
  public: true
  health:
    level: 2
    reasons:
      - Above
  sisense_data:
    chart: 9298897
    dashboard: 482006
    embed: v2
    border: off      


- name: Pay Equality
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Pay Equality is [measured](/handbook/people-group/people-group-metrics/#compensation) by percentage "compa ratio" (+/- 2 points within 100%) for underrepresented groups at GitLab as defined in [Diversity, Inclusion & Belonging](/company/culture/inclusion/#pay-equality).
  target:
  org: People Success
  is_key: true
  public: true
  health:
    level: 0
    reasons:
      - tbd


- name: Percent of team members over compensation band
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: This metric is manually calculated by the Total Rewards Team, and will be moved over once we start using Compass. The Total Rewards Analysts will analyze against how many team members in a division or department are compensated above the bands specific by our Global Compensation policy. To determine this, use the "In Range? (Metrics)" column from the Low Location Factor Reporting and generate a pivot table using a count of "FALSE" per department and division. Add this information to the "Location Factor Graphs/Summary" tab to generate a percentage based on total headcount per department and division as well as the raw number. The number can help explain the percentage if a department or division is small, for example. The weights being used are 0.25 for % over top end of comp band between 0.01% to 4.9%; 0.5 for band between 5% to 9.9%; 0.75 for band 10% to 14.9%; 1 for anything 15%+.  The purpose of weighting how far over someone is from compensation band is to ensure if there are those over comp band slightly, they are not held at the same level as those hired well over rang
  target: <=1%
  org: People Success
  is_key: true
  public: false
  health:
    level: 2
    reasons:
      - Above target, moving in right direction

- name: Spend per Team Member
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: The spend per team member metric is intended to track variances across the company in compensation, discretionary bonuses, promotions, and involuntary attrition. This metric does not have an associated goal as the purpose is not to reduce costs, but instead understand the early indicators of something going wrong or what may be going well. Consistency should be the key evaluator of the KPI. The Promotion Report can be found on the GitLab google drive. Access is limited to the Total Rewards Team and Parul Luthra (Data Team). 
  target:
  org: People Success
  is_key: true
  public: false
  health:
    level: 2
    reasons:
      - Spend per team member increasing with less hires being made

- name: Compliance, data retention and implementation
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: 100% accuracy of data held in BambooHR for all team members where we have an entity or PEO. Not capturing currently, but plan once the team member is hired for this role.
  target:
  org: People Success
  is_key: true
  public: false
  health:
    level: 0
    reasons:
      - tbd

- name: Google drive documentation
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: 100% accuracy and compliance of all People-related data and documents held Google Drive.
  target:
  org: People Success
  is_key: true
  public: false
  health:
    level: 0
    reasons:
      - tbd

- name: Implementation of audits across Team Member Experience tasks
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: 100% implementation of quarterly audits for onboarding, offboarding and career mobility issues.
  target:
  org: People Success
  is_key: true
  public: false
  health:
    level: 0
    reasons:
      - tbd

- name: Country Conversions completed within due date
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: 100% of country conversions completed within due date. 
  target: 
  org: People Success
  is_key: true
  public: true
  health:
    level: 0
    reasons:
      - Ensure workable employment solutions as company scales and matures. 

- name: Increase the locations GitLab can offer work-sponsorship, in alignment with legal and tax
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Increase the locations GitLab can offer work-sponsorship in by 2, in alignment with legal and tax
  target:
  org: People Success
  is_key: true
  public: true
  health:
    level: 0
    reasons:
      - Presently we only offer employment sponsorship to the Netherlands, as a global company we would aim to facilitate more sponsorship opportunities for our team members. 

- name: Complete relocations within due date for eligible relocation requests, in alignment with the people business partners & total rewards.
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: 100% relocations within due date for eligible relocation requests, in alignment with the people business partners & total rewards.
  target:
  org: People Success
  is_key: true
  public: true
  health:
    level: 0
    reasons:
      - Completing relocations in a timely manner ensures compliance, a positive team member experience and reduced room for error for our payroll team. 

- name: Number of All Remote Certifications.
  base_path: /handbook/people-group/people-success-performance-indicators/
  definition: Number of complete certifications of the GitLab All-Remote Certification (Remote Work Foundation) by community members
  target: Greater than 500 certifications per quarter 
  org: People Success
  is_key: true
  public: true
  health:
    level: 0
    reasons:
      - Awaiting full implementation of GitLab's Learning Experience Platform (EdCast) in FY21-Q4. 
    urls:
    - https://docs.google.com/spreadsheets/d/13UKcaaOiejE_DlLEVXY8Muq26ZinxONdD6SELGCkZfs/edit#gid=31432778
