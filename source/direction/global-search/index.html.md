---
layout: markdown_page
title: "Category Direction - Global Search"
description: "GitLab supports Advanced Search for GitLab.com and Self-Managed instances. This provides users with a faster and more complete search."
canonical_path: "/direction/global-search/"
---

- TOC
{:toc}

## Global Search

| | |
| --- | --- |
| Section | [Enablement](/direction/enablement/) |
| Content Last Reviewed | `2020-07-24` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

Thank you for visiting this direction page on Global Search in GitLab. This page belongs to the [Global Search](/handbook/product/categories/#global-search-group) group of the Enablement stage and is maintained by John McGuire([E-Mail](mailto:jmcguire@gitlab.com)).

This strategy evolves, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AGlobal%20Search) on this page. Sharing your feedback directly on GitLab.com or submitting a Merge Request to this page are the best ways to contribute to our direction.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for search, we'd especially love to hear from you.

### Overview
<!-- A good description of what your category is. If there are special considerations for your strategy or how you plan to prioritize, the description is a great place to include it. Please include use cases, personas, and user journeys into this section. -->

GitLab currently supports [Advanced Search](https://docs.gitlab.com/ee/user/search/advanced_global_search.html) for Starter and above self-managed instances. This provides users with a faster and more complete search experience across GitLab. GitLab.com similarly offers Advanced Search for Bronze and above.

We chronicled our journey of deploying Elasticsearch for GitLab.com through several blog posts.
* [2019-03-20 Lessons from our journey to enable global code search with Elasticsearch on GitLab.com](/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [2019-07-16 Update: The challenge of enabling Elasticsearch on GitLab.com](/releases/2019/07/16/elasticsearch-update/)
* [2020-04-28 Update: Elasticsearch lessons learnt for Advanced Search](/blog/2020/04/28/elasticsearch-update/)

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving user journeys as the category progresses through minimal, viable, complete and lovable maturity levels.-->

Advanced Search is targeted at all personas who might use GitLab. However, the largest benefits come to users performing cross-project code search looking for inner sourcing opportunities or exploring the breadth of public projects across GitLab.com.

#### Challenges to address

GitLab is growing and the path to delivering a world-class Git Repo search is evolving quickly. Enterprise Edition Self-Managed customers need to provide their own install of Elasticsearch to connect to GitLab. We will need to be very creative about how to advance Community Edition and GitLab.com Free. Contributions are welcome!

### Maturity

Currently, GitLab's maturity for Search is viable. Here's why:

GitLab's current Advanced Search experience works for some self-managed instances and the experience of getting started has continued to improve.
The UI lacks some basic search capabilities. Using the search generally requires the user to be very exact in what they are looking for. It offers little capability to explore what is in a repo. We have Added Several of these in the past few months and expect to close the gap this year. 

### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top priorities for a few stakeholders. This section must provide a link to an issue or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

We have added a live roadmap that will track the progess as we complete major epics.
[Global Search Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aglobal%20search&label_name[]=Roadmap)

**In Progress: [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505) Our old user interface was designed to provide a minimal amount of complexity as we focused on building and scaling the backend components. We have started adding more rich features that are available with Elasticsearch. I expect that once we have these features available we will do another search UX design that better utilizes these features with a more complete User Flow.
Our customers have told us that having a single tab for all results with filtering to specific scopes would be a better approach to help find content types across the repo. We are also going to start using horizontal space better by adding filtering to the left-hand side of the search page. This will allow us to expand the results list as well as move more results above the fold. 

**Next: [Advanced Search Ranking](https://gitlab.com/groups/gitlab-org/-/epics/3729) We have been using very little customizations to Ranking. Improving this will be key to the making finding information in GitLLab less intensive. We are alosing goint to making indexing more effienct an imrpove the performance of elasticsearch. [Strategy for Elasticsearch index migrations that add data
] (https://gitlab.com/gitlab-org/gitlab/-/issues/234046)

### What is Not Planned right now

Currently there is not a plan to scale beyond the needs of Paid Groups on GitLab.com. This means that while the ambition of the Search Group is to expand Advanced Search to all users of GitLab, we're not yet ready to move in that direction. We will contiune to add features to Baisc search as we can while evolving Advanced Search. 

### The Future of Global Search

As GitLab continues to grow it will become important to keep growing with GitLab. One of these areas that Global search will have impact is in adding new content types. Currently, this includes adding [Vulnerabilities](https://gitlab.com/gitlab-org/gitlab/-/issues/233733), and [Snippets](https://gitlab.com/gitlab-org/gitlab/-/issues/20963) . 
We will also be Allow be planning to offer deeper integration across other list pages in Gitlab.  Lucene based indexing allows for very fast return of structured lists there are several pages in GitLab that offer these list and it’s a continual improvement to improve the speed. 

Increase the number of Self-Managed users, using Advanced Search. We are considering making [Elasticsearch part of the GitLab EE](https://gitlab.com/gitlab-com/Product/-/issues/1716) . We have already made Advanced Search part of SaaS for all paid users. 

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk about, but we’re talking with customers about what they actually use, and ultimately what they need.-->

Both GitHub and BitBucket provide a more comprehensive and complete search for users; particularly in their ability to deeply search code and surface those results to users. While GitLab's Advanced Search is available to self-managed users.

There is a great need to improve the ability to search across repos for all competitors. This is commonly asked about from our prospective customers and is included in the comparison matrix for GitLab as well as the ROI calculator. 

GitHub’s Chief Technology Officer announced that Search is the number one priority. 

{::options parse_block_html="false" /}

<div class="center">

<blockquote class="twitter-tweet"><p lang="en" dir="ltr">It’s 2022. What is the one thing you hope <a href="https://twitter.com/github?ref_src=twsrc%5Etfw">@GitHub</a> has added/removed/improved/changed. <br><br>Side note: we are working on search so feel free to mention though it’s #1 in your hearts and #1 on our list already <a href="https://t.co/84bwBUt5sz">pic.twitter.com/84bwBUt5sz</a></p>&mdash; Jason Warner (@jasoncwarner) <a href="https://twitter.com/jasoncwarner/status/1325499945047126016?ref_src=twsrc%5Etfw">November 8, 2020</a></blockquote> 

<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

</div>



<!-- ### Analyst Landscape -->
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues that will help us stay relevant from their perspective.-->

<!-- ### Top Customer Success/Sales issue(s) -->
<!-- These can be sourced from the CS/Sales top issue labels when available, internal surveys, or from your conversations with them.-->

### Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most thumbs-up), but you may have a different item coming out of customer calls.-->

- [UX Search Enhancements for Advanced Search](https://gitlab.com/groups/gitlab-org/-/epics/1505)

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

- [Advanced Search Operational maturity on GitLab.com and EE](https://gitlab.com/groups/gitlab-org/-/epics/2132)

### Top Strategy Item(s)
<!-- What's the most important thing to move your vision forward?-->

- [Advanced Search Ranking](https://gitlab.com/groups/gitlab-org/-/epics/3729)
