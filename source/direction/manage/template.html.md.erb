---
layout: markdown_page
title: Product Stage Direction - Manage
description: "The Manage stage in GitLab delights business stakeholders and enables organizations to work more efficiently"
canonical_path: "/direction/manage/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Stage Overview
The Manage stage in GitLab **delights business stakeholders and enables organizations to work more efficiently**. Managing a piece of software is more than maintaining infrastructure; tools like GitLab need to be adoptable by companies of all sizes and be easy to operate. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to prevent tools from hindering their velocity.

* **Delighting the business**: to make GitLab adoptable by organizations of any size, it must excel at meeting table stakes that are set by the business. A skyscraper with many people in it can only be enabled by a solid, secure foundation - an application serving a similar scale isn't any different. GitLab needs to support the access control, onboarding, security, and auditing needs that enables enterprise-level scale. We also need to make the foundation easy to lay; constructing a building is slow and arduous when it's done brick-by-brick. Adopting GitLab should be fast and reliable and show a quick trail to getting an amazing return on your investment in GitLab.
* **Working more efficiently**: while we want to fulfill foundational needs, GitLab strives to give you the ability to work in new and powerful ways. We aspire to answer valuable questions for users and to automate away the mundane. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

## Our Groups
Manage is composed of 4 distinct groups that support our mission of **delighting business stakeholders and enabling organizations to work more efficiently**. We plan with 3 timeframes in mind:

* Our **vision** looks ahead ~3 years toward an ambitious future state. Our stage has a vision, and so do each of our groups.
* Our **goals** help provide measurable objectives that are conditions of success for us to realize our vision. We can't realize our stage/group vision without accomplishing these things.
* Finally, **What's Next** are areas of immediate, tactical focus covering the next ~3 releases related to our goals.

All of these sections are intended to be short (no more than a few bullets) and help guide our decisions. They live in parallel with our [category direction pages](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/doc/templates/product/category_direction_template.html.md), which go into more detail on specific capabilities and are not constrained to specific timeframes.
* If an area of interest isn't explicitly mentioned in our Goals or What's Next, please assume that we're not focused on it and won't devote a significant amount of attention to it.

### Access
Access provides leadership on some of GitLab's most foundational capabilities - access control, authentication/authorization, permissions, and subgroups - that enable users to get into GitLab quickly and start getting work done. If we're doing our job correctly, an instance should be able to get a new user onboarded into GitLab without friction: at the right time, with the right level of permissions, to the right resources - all with a great user experience for the new user and the administrator doing the configuration and maintenance.

### Optimize
Optimize helps organizations more quickly recognize the value of their innovations in two ways:
* Providing visibility into value streams and highlighting waste to drive GitLab users toward improvement opportunities
* Increasing the effectiveness and efficiency of knowledge workers in daily work, prompting decisionmaking that maximizes benefit across the whole value stream

Counter-productive local optimizations are a natural result of a limited field-of-view. By creating situtional awareness informed by analytics and value-stream thinking, we give every GitLab user the superpower of extraordinary insight and efficiency.

### Compliance
Historically, managing compliance programs has been complex and unfriendly. GitLab, as a complete DevOps platform, is well-positioned to take the friction out of managing compliance for your organization's activities and processes within GitLab. The data you need is already unified within GitLab and not aggregated from disparate data sources, which makes the compliance management process simple and friendly.

### Import
Import's goal is to make the transition into GitLab seamless. Instances rarely begin completely from scratch; almost every organization has existing repositories, projects, and resources that sit on the outside of any new tool after its been adopted. The more we do to help new instances and users start flying in GitLab, the faster they're able to realize value out of the application - and the longer they'll stay.

## Jobs to be Done

To best frame these problems we’ve compiled a set of Jobs To Be Done (JTBD) that represent the jobs our users are hiring us for. If you’re unfamiliar with JTBDs take a look at [this article](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done). 

<%= partial("direction/jtbd-list", locals: { stage_key: "Compliance" }) %>

## Vision
3 years from now, software will be eating the world faster than ever. As Satya Nadella said, "every company is a software company", reinforcing a trend that's had decades to mature. It's a trend that's only accelerating: exogenous events like COVID-19 are putting even greater emphasis on automation and collaboration, even in [more traditional industries](https://fortune.com/2020/05/11/permanent-work-from-home-coronavirus-nationwide-fortune-100/). Knowledge workers across geographies and industries will thrive, with work becoming more distributed and asynchronous than ever.

While the pie grows, the increasing demand for software increases the spectrum of customer needs in tools like GitLab. New types of customers lead to new requirements - security and compliance, for example - and GitLab will be challenged to continue to expand the needs of these industries and new use cases. DevOps spending is predicted to grow at 23.5% CAGR between 2018 - 2023 (IDC 2019), and a rapidly expanding pie means both catering to these new customers and deepening our relationship with existing personas.

### Vision Themes

#### All-in on SaaS
The growth in DevOps spending is predicted to be led by cloud deployment, and for good reason. All things equal, few organizations want to maintain their own tooling infrastructure. A need for control, compliance, and security compels organizations to self-hosted deployments; over the next 3 years, we'll make progress against each of these needs by:
* Improving isolation and administrative control on GitLab.com to match self-managed deployments.
* Bringing many capabilities currently exclusive to self-managed to GitLab.com, like [LDAP group sync](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html#group-sync).
* Supporting federated architecture patterns, allowing users to work across multiple instances and deployments.

Other stages will support other dimensions of this theme, such as [SaaS reliability](https://about.gitlab.com/direction/enablement/#saas) and [multi-platform support](https://about.gitlab.com/direction/ops/#multi-platform-support).

#### GitLab the business hub
Ultimately, tools that engineers build serve an organization's goal. Whether you're part of a non-profit, a public sector organization, or a for-profit corporation, software is built for a purpose. GitLab's aspiration is to help you measure your progress against that goal better than any other tool. The [DevOps toolchain crisis](https://about.gitlab.com/devops-tools/) is real, and it doesn't stop at software development - it extends to the many tools companies use to accomplish their goals. While our 3-year goal may not to be displace specific tools well beyond the development workflow, our aspiration is to delight a continually broader swathe of personas in our tool. Delighting business-minded personas are next on the list, by:

* Connecting business objectives to your development process, allowing organizations to track initiatives that are moving the needle in a single application.
* Introducing ML-powered insights that identify areas of waste in your process and surfacing anomalous events for security and compliance teams.
* Making GitLab's ROI apparent to executives through great dashboards and reporting.
* Delighting FP&A teams by making finance and accounting valued personas in GitLab, making R&D capitalization and expense reporting easy.

#### Fully managed compliance
According to a 2019 IDC report, only 11% of survey respondents had security and compliance embedded into their DevOps processes. Most see these steps as frustrating, time-consuming bottlenecks that take many people-hours to resolve. Like [security](https://about.gitlab.com/direction/secure/#security-is-a-team-effort), compliance is a team effort - and when shifted left, becomes significantly more painless and cost-effective. We'll build on our compliance roadmap by:

* Extending our compliance posture on GitLab.com for the public sector with FedRAMP authorization and supporting European data residency.
* Providing a lovable permissions and access model for users to keep your GitLab deployment secure and in compliance.
* Preventing configuration drift jeopardizing your compliance posture with alerts and evidence reports that delight auditors.

#### Shortening time-to-value
On theme with a wide variety of industries adopting DevOps, our goal is getting customers into the product, getting them started, and getting out of the way. Our challenge is to make GitLab intuitive and easy to use without a steep learning curve; we've built our application on a foundation of [small primitives](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives), and our goal is to reduce the amount of configuration and setup you need to get your team productive. We'll get users and organizations to their "ah-ha" moment faster by:

* Allowing instances to import from and integrate a wide variety of tools that customers use and love.
* Adopting lovable templates for common use cases throughout the product, teaching our users best practices from industry leaders.
* Making user onboarding lovable and intuitive across a variety of personas.

### Access
Within 3 years, organizations using GitLab will be able to securely and quickly provide access to the right areas of the application at the right level of access. Administrators will have a hands-free, automated experience that minimizes the time spent operating their instance. Administrators will have visibility into who has system access and mechanisms to enforce their security and compliance requirements.

### Optimize
Within 3 years, organizations using GitLab will be able to provably plan, deliver, and deploy defect-free software faster than any of their peers.

### Compliance
We will automate compliance within GitLab to save time for compliance professionals and remove friction for developers, so they can all focus on the most valuable uses of their time. We will make compliance fast, simple, and friendly and reduce the amount of time spent on compliance activities by at least 50%.

GitLab is currently focused on managing compliance for the activities and processes that exist within GitLab. In the future, we would like to provide similar compliance features and automation for the things you ship from GitLab.

This could include Infrastructure as Code (IAC) templates, deployments, and real-time monitoring of your infrastructure compliance.

### Import
Within three years, even the largest organizations will be able to self-service their migration into GitLab from the most common platforms, regardless of their target deployment model.

## Goals for 2021
Through 2021, the Manage stage will provide such compelling, must-have value to large customers that we will be able to **attribute over $100M in ARR to Manage capabilities by end of year**. This means that Manage is a must-have part of the feature set that supports that customer, or Manage was a key part of their adoption journey.

### Goal Themes

#### Enterprise readiness
We're going to focus on increasing and retaining the number of customers with enterprise-scale needs. We're doing this by focusing on:

* Enterprise-grade authentication and authorization. We'll focus on SAML and build excellent compatibility and documentation with large identity providers. This should work on both GitLab.com and self-managed.
* Comprehensive audit events for everything that’s done within GitLab and allowing those events to be accessible via the API and UI.
* Isolation and control, especially for GitLab.com. For some organizations, there must be safeguards in place to prevent users from viewing or accessing other groups and projects. Providing isolation of group managed accounts will help organizations better manage their GitLab usage by providing a more "instance-like" experience at the group level.

| Success factor | How we'll measure |
| ------ | ------ |
| Increased self-managed enterprise adoption | At least X customers over Y paid seats purchasing a self-managed license in 2021 |
| Increased GitLab.com enterprise adoption | At least 15 customers over 500 paid seats purchasing GitLab.com in 2021 |
| Increased engagement with enterprise must-have features | Increase in paid feature engagement for the enterprise: [Paid SAML](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageaccess---paid-gmau---mau-using-paid-saml) |
| Increased CSAT from large organizations | TBD |

##### GitLab.com
Of particular importance to our goal of enterprise readiness is ensuring that GitLab.com is ready to meet the needs of large organizations. By the end 

of 2021, we will close the following gaps between self-managed and GitLab.com:

| Problem | Approach |
| ------ | ------ |
| Since all users on GitLab.com are eligible to be added to a group or project, it's too easy to select the wrong user when adding a new member. | We'll improve the autocomplete behavior when adding a new member by not presenting members that aren't eligible to be added (when using a [domain restriction](https://gitlab.com/gitlab-org/gitlab/-/issues/11878), for example). |
| Some user events lack auditability and visibility. | Audit events are visible at the group level, including a [group-level API](https://docs.gitlab.com/ee/api/audit_events.html). We'll continue to add [additional audit events](https://gitlab.com/groups/gitlab-org/-/epics/736) and improve the [overall usability of these events](https://gitlab.com/groups/gitlab-org/-/epics/418) over time. |
| Individual members of an organization can fork enterprise projects into their personal namespace, introducing security concerns over IP controls. | Groups can already [prevent forks](https://gitlab.com/gitlab-org/gitlab/-/issues/216987) outside their namespace. In the future, [user personas](https://gitlab.com/gitlab-org/gitlab/-/issues/239368) will allow organizations to manage project forks, even if they're in personal namespaces. |
| Organizations want to have traceability (and control) over the actions a user's account is taking. | While we don't have plans for an organization to have control over all actions a user's account is taking (a single user account can belong to multiple paid groups; defining account ownership in GitLab.com's current architecture is very challenging), we will allow an enterprise to have [visibility and manage user activity that is directly relevant to their organization's group](https://gitlab.com/groups/gitlab-org/-/epics/4345). |
| Since GitLab, Inc. employees are the only admins on GitLab.com, administrative controls for group owners are limited. | We're tentatively planning to pull instance-level admin activity into an [administrative area for group owners](https://gitlab.com/gitlab-org/gitlab/-/issues/209020). |
| Managing credentials (e.g. regular rotation) is not possible, since users belong to the instance and not a particular namespace. | We will allow users to create group specific credentials that can be managed by administrators in a group specific version of [Credentials Inventory](https://gitlab.com/groups/gitlab-org/-/epics/4123)|
| Since instance-level functionality is admin-only, group owners can't use any features built at this level. | TBA |

If you'd like to comment on our GitLab.com approach, please consider opening an MR or leaving a comment in [this feedback issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17277).

#### Enhancing Ultimate/Gold
We're going to drive an Ultimate/Gold story that creates obvious, compelling value. The majority of Ultimate's value proposition lies in application security, but we'll strive to improve the breadth of this tier by driving more value in Ultimate, such as:

* Improving tools that help compliance-minded organizations thrive. GitLab makes it easy to contribute, but administrators should have comprehensive control to establish, enforce, and provide evidence of organizational policies that are part of a compliance program or framework. Our compliance vision will evolve to introduce features that enable organizations to rely on GitLab for the enforcement and documentation of policies they set.
* More customizable and fine-grained permissions. GitLab's RBAC permissions system works well for most, but we should offer more powerful customization for customers to leverage.
* Powerful analytical insights. Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.

Success in this theme looks like:
* Increased share of IACV in Ultimate/Gold.
* Increased adoption and engagement with Ultimate-level features.

#### Easy adoption
Manage will create easy paths to support our land-and-expand strategy. There's a starting point for any organization with an expansive new tool, and Manage will make this transition easy by supporting natural starting points - ideally in Core, for all groups - that get our customers started and hooked on GitLab:
* Easier import at any scale. Large-scale moves to GitLab should be significantly easier. We'll particularly focus on the user experience migrating from 2-3 key competitors, including gracefully recovering from failures.
* Drive entry-level enterprise table stakes into Core. Each group will focus on a Core value proposition that allows every user to get value - and encourages enterprises testing the water to land (and later expand) in GitLab.

### Access
1. Currently, self-managed and GitLab.com have different authentication and authorization capabilities. Our goal for 2021 is to bring features to Gitlab.com to unblock adoption; resulting in at least 15 enterprise-scale customers over 500 paid seats to begin using GitLab.com in 2021.
2. Implement fine-grained user permissions for common enterprise use cases, particularly segregation of duties.
3. Build support for the top 5 SAML SSO and SCIM providers to reduce time and friction onboarding customers.

Access uses a [single epic](https://gitlab.com/groups/gitlab-org/-/epics/3134) to highlight issues we're prioritizing or refining. If you're not confident an important Access issue is on our roadmap, please feel free to highlight by commenting in the relevant issue and @ mentioning the relevant PM.

### Optimize
In order to reach our vision, we need to:
* Create at least 1 analytics feature that our customers consistently use. Our analytics features are still trying to find product-market fit; our goal is to find that fit by finding a feature that our customers find useful and return to.
* Solve 1 major problem for the large enterprise with an MVC and at least 1 iteration. The problem most organizations face is a lack of instance-level visibility and an unknown ROI from their GitLab deployment. We'd like to ship against this problem and validate that we're moving in the right direction with these target customers.

### Compliance
Towards this vision, the Compliance group at GitLab is focused on three key areas:
* Enabling organizations to enforce compliance controls inside of GitLab (e.g. Define MR approvals at a global level and only authorized users may change those settings), including separation of duties (described in more detail below)
* Aggregating evidence and other audit information in a way that's easy to obtain and read
* Ensuring a comprehensive level of traceability and auditability of GitLab using Audit Events

### Import
Following our vision, the Import group is focused on these goals for 2021:
* Achieving a Lovable GitLab-to-GitLab migration experience. Allow a self-managed instance to easily migrate to GitLab.com (and vice versa) and self-service a migration of projects, groups, users, and most other objects.
* Double our import activity from GitHub. We'll accomplish this by improving the import user experience, performance, and capabilities of our GitHub importer (such as extending our support for GitHub Enterprise and introducing the ability to import an entire [GitHub organization](https://gitlab.com/gitlab-org/gitlab/-/issues/16911)).
* Share of failed imports reduced by 50% (from X to Y)

## What's Next

### Access
1. Enable support for "secret" groups through a ["no access" role](https://gitlab.com/gitlab-org/gitlab/-/issues/220203)

2. Improve the GitLab.com administration experience with [user personas](https://gitlab.com/gitlab-org/gitlab/-/issues/218631) and group-specific credentials. Melissa Ushakov describes our approach:

<iframe width="560" height="315" src="https://www.youtube.com/embed/U5iO3nyajWc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

3. Policies MVC to deny [project deletion](https://gitlab.com/gitlab-org/gitlab/-/issues/34693) and [membership management](https://gitlab.com/gitlab-org/gitlab/-/issues/234099).

4. [SAML group sync MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/118)

#### What we're not doing
* Improvements to user profiles and user management, as defined on the [Users direction page](https://about.gitlab.com/direction/manage/users/#users)

### Optimize
In pursuit of these goals, Optimize is currently focused on 3 initiatives:

1. [Dogfooding](https://gitlab.com/groups/gitlab-org/-/epics/3894): focus on internal customers by prioritizing improvements that increase the efficiency of engineering teams. By succeeding with our internal customers and refocusing the group's efforts on [dogfooding](https://about.gitlab.com/handbook/product/product-processes/#dogfood-everything), we'll also delight other engineering teams with powerful new capabilities. The value of this work should be immediately apparent to GitLab's Engineering and Quality Departments, and also help our customers track and optimize their engineering processes.

2. [Enterprise DevOps reporting](https://gitlab.com/groups/gitlab-org/-/epics/4066): understanding instance-level adoption of GitLab is a blind spot for customers of large instances. We'd like to solve for two main problems for executives and leaders: tracking GitLab's ROI and helping find centers of excellence. We'll prioritize work like an [instance-level MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/193435).

3. Value stream analytics: we're also prioritizing Value Stream Analytics as the centerpiece of value stream management in GitLab, and we're pursuing improvements to make this feature easily understood and immediately useful.

### Compliance
* Increase the number of `workflow::ready for development` issues by spending 50% of our time on issue refinement each milestone.
* Continuing our work to [refactor audit events](https://gitlab.com/groups/gitlab-org/-/epics/2765) to improve overall performance and reduce implementation complexity
* Implement necessary product performance indicator instrumentation to help us make data-informed decisions about our product roadmap and shipped features
* Iterate on our [Audit Reports MVCs](https://gitlab.com/groups/gitlab-org/-/epics/2955) to add more value for some high-demand reports

To ensure we build a cohesive Compliance experience at GitLab, the following points of view are a baseline for other GitLab product groups to consider.

### Import
* Viable GitLab Migration, including Groups, Subgroups, and Projects.
* Scaling the GitHub importer user experience to support migrating an organization with more than 1,000 projects.
* Instrumenting our ability to track and troubleshoot import failures.

## Pricing
To support our goals in 2020 and our 3-year strategy, Manage's focus will skew towards paid tiers. Electing to focus on enterprise-level themes like compliance and value stream management is intended to drive [company-level financial goals](https://about.gitlab.com/company/okrs/), and we'll prioritize valuable features that customers using GitLab can land and expand into.

We'll also prioritize an approach that doesn't require an enterprise-grade customer to immediately pay for GitLab to realize value. Customers of any size should be able to adopt GitLab and fall in love with it, for free.

### Core/Free
Each Manage group should use our Core/Free tier as the primary way to build GMAU and allow our target customer to land in our product. For the most part, features that tend to be "table stakes" for organizations of any size should land in Core/Free - we won't gate GitLab adoption behind a paid tier.

Noting that our [pricing strategy](https://about.gitlab.com/handbook/ceo/pricing/#four-tiers) directs Core/Free toward the individual developer, we also want to ensure that enterprises can quickly realize value from Manage before expanding further. Examples:

* Importing capabilities (such as [project](https://docs.gitlab.com/ee/user/project/import/) and [group](https://docs.gitlab.com/ee/user/group/settings/import_export.html) import/export) will always be free. Widely available import capabilities will increase the number of GitLab instances, decrease time to value, and drive SMAU.
* Templates that help bootstrap new instances. While custom templates that help organization-specific needs will remain in paid tiers, we should ensure that new instances can get up and running quickly with out-of-the-box projects and groups that make GitLab's value easy to discover.
* While some authentication features like [group sync](https://docs.gitlab.com/ee/administration/auth/ldap/#group-sync) will be paid, configuring and using basic authentication strategies (LDAP, SAML) will be free.

### Starter/Bronze
Not a significant part of Manage's pricing strategy, we'll use this tier to appeal to managers of small teams by positioning capabilities into this tier that help with team management. Managers of teams can benefit from project-level capabilities that help them manage their direct team, but most of Manage's future roadmap is targeted toward directors of larger enterprises in Premium. Examples:
* Project-level analytics and events that serve individual managers/maintainers, such as [code review analytics](https://docs.gitlab.com/ee/administration/audit_events.html#project-events).

### Premium/Silver
The default paid tier for enterprises, Premium will cater to directors operating a medium to large instance. We'll direct features that solve for typical entry-level enterprise needs: reporting and analytics, operational efficiency, security and compliance, and other needs that are must-haves for medium to large organizations. While this type of organization should be able to get started in GitLab at lower tiers, they won't be able to thrive at scale. Examples:

* Authentication/authorization capabilities that make managing users easy at scale. While the task of onboarding/offboarding a single user is straightforward for managers, we need to manage this aspect of operations consistently and automatically for instances with thousands of users. We anticipate features like [LDAP group mapping](https://docs.gitlab.com/ee/user/group/index.html#creating-group-links-via-filter) and [SCIM](https://docs.gitlab.com/ee/user/group/saml_sso/scim_setup.html) to remain in this tier.
* Individual compliance controls. For organizations experienced and opinionated on managing their own compliance stance, we anticipate offering many individual compliance controls in Premium/Silver for organizations to use. While we're looking to build a comprehensive, end-to-end compliance solution in Ultimate, enterprises should have access to individual tools in Premium. This includes a comprehensive set of [audit events](https://docs.gitlab.com/ee/administration/audit_events.html#impersonation-data), [auditor users](https://docs.gitlab.com/ee/administration/auditor_users.html), and [IP access restriction](https://docs.gitlab.com/ee/user/group/#ip-access-restriction).
* Organizational tools that help individual teams stay efficient, without requiring an administrator. [Project templates](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html) - and (in the future) group templates help teams stay efficient. Eventual iteration on subgroups to serve the need for teams in GitLab will likely fall to this tier.

### Ultimate/Gold
Directed toward an executive likely buyer, Manage will direct capabilities into Ultimate/Gold that serve the organizational needs of the complex enterprise operating a large GitLab instance. The difference between a Premium and Ultimate feature is operational efficiency: a large enterprise instance has everything they need to thrive in Premium, but Ultimate features automate and make that same instance much easier to manage, optimize, and operate. Examples:

* Compliance capabilities, especially those that introduce compliance steps directly into the developer workflow. These are typically handled with significant people-hours and effort outside the core product, with a huge ROI when compliance tasks can be automated away.
* Audit reporting, where many individual events are aggregated into a comprehensive report for the benefit of a 3rd party. While evidence of individual events will be in lower paid tiers to serve security-oriented use cases (e.g. data was exfiltrated yesterday, and I need to understand what happened), we see great value in being able to generate a comprehensive compliance report for the benefit of an auditor.
* Intelligent alerting that use predictive analytics to identify important events. These events may include instance events that lie outside of a compliance framework (e.g. creating an issue and assigning it to an administrator on a new IP address failing an MFA check) or events driven by your value stream (e.g. an MR review is taking much longer than usual and may need to be broken down).

## Metrics
Manage uses [Stage MAU](https://about.gitlab.com/handbook/product/metrics/#stage-monthly-active-users-smau) as a primary measure of success. This represents the unique number of users getting value from the stage; all groups should be able to contribute to improving this number.

Manage's Stage MAU is currently being improved. Please see [this issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17231) to track progress.

Individual groups track progress against a number of group-specific [performance indicators](https://about.gitlab.com/handbook/product/metrics/):

| Group | Dashboard URL |
| ------ | ------ |
| Access | https://app.periscopedata.com/app/gitlab/636494/ |
| Optimize | TBC |
| Compliance | https://app.periscopedata.com/app/gitlab/663045/ |
| Import | https://app.periscopedata.com/app/gitlab/661967/ |

## How we operate
Manage operates under GitLab's values, but is a stage that seeks to particularly excel in certain areas that support our goals above. We seek to be leaders at GitLab by:

### Iterate on the essential
* Leading the way on iteration, regularly shooting for small but ambitious MVCs.
* Supporting iteration with a great planning and development process, giving us checkpoints to keep issues small and incremental. As a result, our throughput is high.
* Valuing the 1-year themes above, and deliberately deciding to not pursue initiatives that don’t support our 2020 goals. We'd rather do a few things well than a bunch of things poorly.
* Prioritizing depth over breadth. For the most part, we’re biased toward doubling down and investing on what’s working rather than extending the breadth of our stage.

### Measure what matters
* Prioritizing instrumentation through our North Star dashboards, which we regularly monitor to keep our priorities in check.
* Measuring business value by tying customer delight and revenue to our priorities.

### Great team
* Aspiring to be the happiest team at GitLab, with high individual job satisfaction.
* Having great work-life balance, ensuring that we [value friends and family above work](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) and avoid individual burnout.

You can track our operational goals [here](https://gitlab.com/groups/gitlab-org/-/epics/3245).

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
