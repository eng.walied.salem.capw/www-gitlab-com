---
layout: markdown_page
title: "Category Direction - GitLab Pages"
description: GitLab Pages allows you to create a statically generated website from your project that is automatically built using GitLab CI and hosted on our infrastructure.
canonical_path: "/direction/release/pages/"
---

- TOC
{:toc}

## GitLab Pages

GitLab Pages allows you to create a statically generated website from your project that
is automatically built using GitLab CI and hosted on our infrastructure.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3APages)
- [Overall Vision](/direction/release)
- [Documentation](https://docs.gitlab.com/ee/user/project/pages/) 
- [UX Research](https://gitlab.com/gitlab-org/ux-research/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3APages) - [Research Insights](https://gitlab.com/gitlab-org/uxr_insights/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=Category%3APages)

### Overall Prioritization

Pages is a popular
feature and one that people really enjoy engaging with as part of the GitLab
experience; it's truly one of our most "personal" features in the Release stage.
We do not plan to provide a market-leading solution for static web page hosting,
but we do want to offer one that is capable for most basic needs, in particular
for hosting static content and documentation that is a part of your software
release. 

Popular issues and compelling blockers for our users hosting static content and documentation will be the top priority for Pages. For more information about how GitLab is improving the experience for managing static site content - please review our [Static Site Editor Direction page](/direction/create/static_site_editor/). 

## What's Next & Why
 
We are tackling the Object Storage re-architecture to support cloud native installations of GitLab Pages via [gitlab#39586](https://gitlab.com/gitlab-org/gitlab/issues/39586). We are working on transitioning GitLab Pages from NFS to Object Storage in [gitlab&3901](https://gitlab.com/groups/gitlab-org/-/epics/3901). We are currently working on making zip cache configurable
via [gitlab-pages#464](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/464)

## Maturity Plan

This category is currently at the "Complete" maturity level, and our next maturity target is "Lovable" (see our [definitions of maturity levels](/direction/maturity/)).

Key deliverables to achieve this are:

- [Automatic certificate renewal](https://gitlab.com/gitlab-org/gitlab-foss/issues/28996) (Complete)
- [Access controls for Pages on gitlab.com](https://gitlab.com/gitlab-org/gitlab/issues/25362) (Complete)
- [Speed up Pages initialization time by using configuration API](https://gitlab.com/gitlab-org/gitlab/issues/28782) (Complete)
- [Technical Evaluation for Object Storage](https://gitlab.com/gitlab-org/gitlab/-/issues/208135) (Complete)
- [Transition GitLab Pages from NFS to Object Storage](https://gitlab.com/groups/gitlab-org/-/epics/3901) (13.4 to 14.0)
- [Pages Migration off of NFS](https://gitlab.com/groups/gitlab-org/-/epics/3901) (13.9)
- [Remove Disk Source Configuration](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/382) (14.0)
- [Redirect from GitLab pages URL to custom domain when one exists](https://gitlab.com/gitlab-org/gitlab/issues/14243) 
- [Pages without DNS wildcard](https://gitlab.com/gitlab-org/gitlab/issues/17584) 
- [Per-site, in-repo configuration](https://gitlab.com/gitlab-org/gitlab-pages/issues/57)
- [Multiple version support](https://gitlab.com/gitlab-org/gitlab/issues/16208)

## Competitive Landscape

We are invested in supporting the process of developing and deploying code from a single place as a convenience for our users. Other providers, such as [Netlify](https://www.netlify.com/), deliver a more comprehensive solution. There are project templates available that offer the use of [Netlify for static site CI/CD](https://gitlab.com/pages?filter=netlify), while also still taking advantage of GitLab for repository, merge requests, issues, and everything else. GitLab offers configurable redirects, a well-loved featured of Netlify, made available in [gitlab-pages#24](https://gitlab.com/gitlab-org/gitlab-pages/-/issues/24).

We are seeing a rise in [JAMStack](https://jamstack.org/) and static site generators partnering in the media. This trend toward API-first, affirms our modernization effort of Pages, reinforcing our cloud native installation maturity plan. 

## Top Customer Issue(s) and Top Customer Success/Sales Issue(s)

The most popular customer issue is ([gitlab#17584](https://gitlab.com/gitlab-org/gitlab/issues/17584)).
Creating Gitlab pages today requires admins to setup wildcard DNS records and SSL/TLS certificates. Some services
and/or corporate security policies forbid wildcard DNS records, preventing users from
benefitting from using Gitlab Pages. This issue will remove the need for wildcard DNS and allow
many more users to enjoy the Pages experience.

We are working on better supporting custom domains and will expect to have a path to redirect to custom domains ([gitlab#14243](https://gitlab.com/gitlab-org/gitlab/issues/14243)) following the Pages re-architecture effort. 

Our most  popular issue for Pages, Multiple-version Pages support ([gitlab#16208](https://gitlab.com/gitlab-org/gitlab/issues/16208)) will also need to wait until the new architecture is in place, but we are working on an alternative solution of using environments for achieving the same functionality ([gitlab#33822](https://gitlab.com/gitlab-org/gitlab/issues/33822)). 

## Top Internal Customer Issue(s)

Our top internal customer issue is ([gitlab#16208](https://gitlab.com/gitlab-org/gitlab/issues/16208)) which enables having multiple GitLab Pages generated based on branches or tags. We have been seeing more demand for cloud native, kubernetes based delivery of GitLab.com which GitLab Pages NFS storage is a blocker for, this will be addressed via [gitlab#39586](https://gitlab.com/gitlab-org/gitlab/issues/39586).

## Top Vision Item(s)

Adding Review Apps for Pages ([gitlab#16907](https://gitlab.com/gitlab-org/gitlab/issues/16907)) will allow for more sophisticated development flows involving testing and review of Pages deployments. Enhancing the maturity of deployment would integrate Pages more critically within projects and groups. 

The [Static Editor Team](https://docs.gitlab.com/ee/user/project/static_site_editor/) has shown interest in becoming more competitve with Netlify, using the GitLab Pages engine which is discussion in [gitlab#17539](https://gitlab.com/gitlab-org/gitlab/-/issues/17539).

Another vision item being investigated is to leverage JAMstack for Pages. The primary goal would be to enhance the user experience ([gitlab#2179](https://gitlab.com/groups/gitlab-org/-/epics/2179)) and allow easy to set up Pages from the UI without expanding APIs. Lastly, in combination with [feature flags](/direction/release/feature_flags), Pages can be used to support A/B testing ([gitlab#14122](https://gitlab.com/gitlab-org/gitlab/issues/14122)). 
