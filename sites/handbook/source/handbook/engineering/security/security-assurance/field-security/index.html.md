---
layout: handbook-page-toc
title: "Risk and Field Security Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc .hidden-md .hidden-lg}
 
## Risk and Field Security Mission
 
The Risk and Field Security team serves as the public representation of GitLab's internal Security function. The team is tasked with providing high levels of security assurance to internal and external customers. We work with all GitLab departments to document requests, analyze the risks associated with those requests, and provide value-added remediation recommendations.

Hear more about our team and how we support GitLab in this [interview with the Manager of Risk and Field Security.](https://www.youtube.com/watch?v=h95ddzEsTog) 
 
## Risk and Field Security Primary Functions

```mermaid
graph TD
subgraph Legend
	Link[These are clickable boxes]
	end
  A[Risk and Field Security Team<br/>Handbook Page<br/>You are here!] -->B
  A-->C
  A-->D
  B[Customer Assurance Activities Procedure]
  B-->E[Customer Assruance Package]
  B-->F[Turst Center]
  B-->G[AnswerBase]
  C[Third Party Risk Management Procedure]
  D[Security Operataional Risk Management Procedure]
 
	  style A stroke: #db3b21, stroke-width:5px
    style B stroke:#6e49cb, stroke-width:2px 
    style C stroke:#6e49cb, stroke-width:2px 
    style D stroke:#6e49cb, stroke-width:2px 
    style E stroke:#6e49cb, stroke-width:2px 
    style F stroke:#6e49cb, stroke-width:2px 
    style G stroke:#6e49cb, stroke-width:2px 
    style Link stroke:#6e49cb, stroke-width:2px	

click B "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html" "Customer Assurance Activities"
click C "https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html" "Third Party Risk Management"
click D "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/risk-management.html" "Secuirty Operational Risk Management"
click E "/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html" "Customer Assurance Pacakge"
click F "https://about.gitlab.com/security/" "Trust Center"
click G "https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/common-security-questions.html" "AnswerBase"

````
<!-- blank line -->
----
<!-- blank line --> 
#### Field Security
In support of GitLab’s Sales and Customer Success Teams, the Risk and Field Security team maintains the [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html) for the intake, tracking, and responding to GitLab Customer and Prospect Security Assurance Activity request. This includes, but is not limited to: 
 
- Proactively maintaining self-service security and privacy resrouces including the [Customer Assurance Pacakge](/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html), the [Trust Center](https://about.gitlab.com/security/") and the **internal only** [Answer Base](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/common-security-questions.html).
- Completing Security, Privacy, and Risk Management Questionnaires
- Assisting in Contract Reviews
- Participating in Customer Meetings
- Providing External Evidence (such as GitLab’s SOC2 Penetration Test reports)
 
Requests for Customer Assurance Activities should be submitted using the **Customer Assurance** workflow in the `#sec-fieldsecurity` Slack Channel. Detailed work instructions are located in the [Field Security Project](https://gitlab.com/gitlab-com/gl-security/security-assurance/field-security-team/risk-field-security#customer-assurance-activities). 
 
 <!-- blank line -->
----
<!-- blank line -->
#### Third Party Risk Management
Whenever a Third Party is introduced into the GitLab environment, there is a risk that their security posture can negatively impact GitLab. In order to reduce this risk, the Risk and Field Security team maintains the [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html) for the intake, assessment, tracking and responding to GitLab Third Party Risk Management requests. 

<!-- blank line -->
----
<!-- blank line -->
#### Security Operational Risk Management
Focused on Tier 2/Operational Risks, the Risk and Field Security team maintains the [Security Operational Risk Management Procedure- StORM](/handbook/engineering/security/security-assurance/field-security/risk-management.html) for the intake, assessment, tracking and responding to GitLab StORM requests. 
<!-- blank line -->
----
<!-- blank line --> 
## Metrics and Measures of Success

`In Progress`
 
## Makeup of the team
- [Devin Harris](/company/team/#dsharris) @dsharris
- [Jennifer Blanco](https://about.gitlab.com/company/team/#jblanco2) @jblanco2
- [Marie-Claire Cerny](https://about.gitlab.com/company/team/#marieclairecerny) @marieclairecerny 
- [Steve Truong](https://about.gitlab.com/company/team/#sttruong) @sttruong
- [Meghan Maneval](https://about.gitlab.com/company/team/?department=security-department#mmaneval20) @mmaneval20
- [Julia Lake](https://about.gitlab.com/company/team/#julia.lake) @julia.lake
 
## References
* [Customer Assurance Activities Procedure](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html)
* [Third Party Risk Management Procedure](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html)
* [Security Operational Risk Management Procedure](/handbook/engineering/security/security-assurance/field-security/risk-management.html)
 
## Contact the Field Security Team
* Email
  * `fieldsecurity@gitlab.com`
 
* Primary Slack Channels
  * `#sec-fieldsecurity`
  * `#sec-risk-mgmt`
