---
layout: markdown_page
title: "Security Terms Glossary"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Security Audit
A comprehensive examination of a Security program, Security Relevant System or Security Controls. Security Audits can be internal or external (3rd party). Security Audits are more comprehensive than security assessments as they require access to trusted information. It is important to understand the scope and covered period of a Security Audit to correctly interpret results.

### Internal Security Audit
A Security Audit conducted by personnel under the employ of the organization conducting the Audit. For example the [Internal Audit Team](/handbook/internal-audit/) and [Security Compliance Team](/handbook/engineering/security/security-assurance/security-compliance/compliance.html) at GitLab conduct Internal Audits of GitLab's Security Program.

### External Security Audit (3rd Party Security Audit)
A Security Audit conducted by a contracted and independent 3rd party. For example GitLab routinely undergoes [SOC2](/handbook/engineering/security/security-assurance/security-compliance/certifications.html) audits and [Penetration Testing](/security/#external-testing) from independent 3rd party auditors.

## Security Assessment
A Security Assessment is an activity in which a Security Program or portions thereof are investigated for fit and function. For instance GitLab conducts [Third Party Security Reviews](/handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html) of our vendors. Security Assessments are less involved than a Security Audit and are generally conducted by an organization who is intending to procure services from another organization. GitLab supports Security Assessments for customers by publishing and maintaining the [Customer Assurance Package](/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html). Customers and Prospects above a certain threshold can receive additional Security Assessment Support through the [Customer Security Assessment Workflow](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html).

## Security Questionnaire
A document that is meant to provide an overview of a Security Program or portions thereof. Security Questionnaires are routinely used during Security Assessments. An example of an industry standard security questionnaire includes the [CAIQ](https://cloudsecurityalliance.org/star/registry/gitlab/) which GitLab makes publicly available.
