---
layout: handbook-page-toc
title: "Technical Account Management Handbook"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Handbook directory

[TAM Segments and Associated Metrics](/handbook/customer-success/tam/customer-segments-and-metrics/)

### TAM responsibilities

- [TAM Team Metrics Overview (VIDEO)](https://www.youtube.com/watch?v=9b8VviLG3yE&t=2s)
- [TAM Skills Matrix](https://docs.google.com/spreadsheets/d/1_UEke64Qkz8wSyqfr_E9qqeAF6rX77w4vIH84Ckm_ts/edit?usp=sharing) (Gitlab Internal)
- [Using Gainsight](/handbook/customer-success/tam/gainsight/)
- [TAM Responsibilities and Services](/handbook/customer-success/tam/services/)
- [TAM and Product Interaction](/handbook/customer-success/tam/product/)
- [TAM and Support Interaction](/handbook/customer-success/tam/support/)
- [Escalation Process](/handbook/customer-success/tam/escalations/)
- [TAM-to-TAM Account Handoff](/handbook/customer-success/tam/account-handoff/)
- [TAM READMEs](/handbook/customer-success/tam/readmes/) (Optional)

### Customer journey

- [Account Engagement](/handbook/customer-success/tam/engagement/)
   - [Non-Engaged Customer Strategies](/handbook/customer-success/tam/engagement/Non-engaged-customer-strategies/)
- [Account Onboarding](/handbook/customer-success/tam/onboarding/)
- [Digital Journey](/handbook/customer-success/tam/digital-journey/)
  * [Nominating Contacts for the Digital Journey](/handbook/customer-success/tam/digital-journey/nominating-contacts-for-the-digital-journey/)
- [Cadence Calls](/handbook/customer-success/tam/cadence-calls/)
- [Success Plans](/handbook/customer-success/tam/success-plans/)
   - [Stage Adoption Metrics](/handbook/customer-success/tam/stage-adoption/)
   - [Developer & Productivity Metrics](/handbook/customer-success/tam/metrics/)
   - [Sample Questions & Techniques for Getting to Good Customer Metrics](https://about.gitlab.com/handbook/customer-success/tam/success-plans/questions-techniques/)
- [Usage Ping](/handbook/customer-success/tam/usage-ping-faq/)
- [Playbooks](/handbook/customer-success/playbooks/)
- [Executive Business Reviews (EBRs)](/handbook/customer-success/tam/ebr/)
- [Customer Renewal Tracking](/handbook/customer-success/tam/renewals/)
- [Customer Health Assessment and Triage](/handbook/customer-success/tam/health-score-triage/)

### TAM managers

- [TAM Manager Processes](/handbook/customer-success/tam/tam-manager/)
- [TAM Manager QBR Template](https://docs.google.com/presentation/d/1P7Cao5xgILSSrpEGy7Sh09djilnbbIK91IuTs-Xq7mY/edit?usp=sharing) (Gitlab Internal)
- [TAM Promotion Template](https://docs.google.com/document/d/1Hg16QVYB2qm0gUGR6H_NPz0cd8OGEuvi3GyDtOsEagY/edit?usp=sharing) (Gitlab Internal)


- - -

## What is a Technical Account Manager (TAM)?

GitLab's Technical Account Managers serve as trusted advisors to GitLab customers. They offer guidance, planning and oversight during the technical deployment and implementation process. They fill a unique space in the overall service lifecycle and customer journey and actively bind together sales, solution architects, customer stakeholders, product management, Professional Services Engineers and support.

See the [Technical Account Manager role description](/job-families/sales/technical-account-manager/) for further information.

### Advocacy

A Technical Account Manager is an advocate for both the customer and GitLab. They act on behalf of customers serving as a feedback channel to development and shaping of the product. In good balance, they also advocate on behalf of GitLab to champion capabilities and features that will improve quality, increase efficiency and realize new value for our customer base.

#### Management

Technical Account Managers maintain the relationships between the customers and GitLab. Making sure that everyone is working towards pre-defined goals and objectives.

#### Growth

Technical Account Managers help to bring GitLab to all aspects of your company, not just software development. They can do this by showing other business unit's how to use GitLab for their day-to-day tasks and to advocate for new features and functionality that are in demand by other groups.

#### Success

Technical Account Managers make sure that the adoption of GitLab is successful at your company through planning, implementation, adoption and training.


## Responsibilities and Services

#### Book of Business

Technical Account Managers are responsible for managing a portfolio of customer accounts based on the sum of the annual contract value of the accounts under management. The current targets per TAM based on segment are:

- Large / Enterprise: $3.25M/TAM
- Commercial: $4M/TAM
- SMB: N/A as it's digitally-led

The team continues to drive efficiency improvements to allow TAMs to effectively manage a larger book of business.

Customers who have an annual recurring revenue (ARR) of $50k or more are assigned a designated TAM, and we are piloting a customer onboarding program for customers in the Commercial segment that have an ARR of $20-50k. We are rolling out our digital enablement program, with a view to enable all paying >20k ARR customers with digital content in FY22.


There are various services a Technical Account Manager will provide to ensure that customers get the best value possible out of their relationship with GitLab and their utilisation of GitLab's products and services. These services include, but are not limited to:

## Relationship Management

- Regular [cadence calls](/handbook/customer-success/tam/cadence-calls)
- Regular open issue reviews and issue escalations
- Account [health assessment](/handbook/customer-success/tam/health-score-triage/)
- [Executive business reviews](/handbook/customer-success/tam/ebr)
- Success strategy roadmaps - beginning with a 30/60/90 day success plan
- To act as a key point of contact for guidance, advice and as a liaison between the customer and other GitLab teams
- Own, manage, and deliver the customer onboarding experience
- Help GitLab's customers realize the value of their investment in GitLab
- GitLab Days

## Training

- Identification of pain points and training required
- Coordination of demo sessions, potentially delivered by the Technical Account Manager or Solution Architect if time and technical knowledge allows
- "Brown Bag" sessions
- Regular communication and updates on GitLab features
- Product and feature guidance - new feature presentations

## Support

- Upgrade planning assistance in conjunction with Support
- User adoption strategy
- Migration strategy and planning in conjunction with Support
- Launch support
- Monitors support tickets and ensures that the customer receives the appropriate support levels
- Support ticket escalations

### Book of Business Prioritization

On the Gainsight Attributes section, the TAM can set the priority level per customer with levels 1-3, with 1 being the highest. Definitions include:

1. High Touch: TAM-led onboarding, regular cadence calls, full TAM-led customer lifecycle
1. Medium Touch: TAM-led onboarding, quarterly check-ins, renewal touch point
1. Digital Touch: No direct TAM involvement, onboarding and enablement driven by email-led digital journey

Why do we use a prioritization system?

- Use in SALSATAM meetings to ensure team alignment
- Managers have more visibility into the potential workload of the team member via more context on the makeup of the overall portfolio
- TAMs have more insight into their portfolio and where they should be spending their time
- TAMs can run associated campaigns of education to low touch or less engaged customers or strategic (high touch) customers to be automatically invited to Customer Advisory Boards or events
- Used to further segment customers, beyond the [Sales Segmentation](/handbook/business-ops/resources/#segmentation)

Criteria to apply includes:

- Future account growth
- Size of the account
- Risk / health of account

The `TAM Portfolio` Dashboard is used to help highlight and review each client, including their priority level.

## TAM Tools

The following articulates where collaboration and customer management is owned:

1. [**Account Management Projects**](https://gitlab.com/gitlab-com/account-management): Shared project between GitLab team members and customer. Used to prioritize/plan work with customer.
1. [**Google Drive**](https://drive.google.com/drive/u/0/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U): Internal. Used to capture call notes and other customer related documents.
1. [**Chorus**](/handbook/business-ops/tech-stack/#chorus): Internal. Used to record Zoom calls.
1. [**Gainsight**](/handbook/customer-success/tam/gainsight/): Internal. Used to track customer health score, logging [customer activity](/handbook/customer-success/tam/gainsight/timeline/#activity-types) (i.e. calls, emails, meetings)

## Related pages

- [Dogfooding in Customer Success](/handbook/customer-success/#dogfooding)
- [Customer Success & Market Segmentation](/handbook/customer-success/#customer-success--market-segmentation)
- [Responsibility Matrix and Transitions](/handbook/customer-success/#responsibility-matrix-and-transitions)
- [Commercial Sales Customer Success](/handbook/customer-success/comm-sales/)
- [Customer Success' FAQ](/handbook/customer-success/faq/)
- [Using Salesforce within Customer Success](/handbook/customer-success/using-salesforce-within-customer-success/)
- [Customer Success Vision](/handbook/customer-success/vision/)
- [GitLab Positioning](/handbook/positioning-faq/)
- [Product Stages and the POCs for each](/handbook/product/categories/#devops-stages)
- [How to Provide Feedback to Product](/handbook/product/how-to-engage/#feedback-template)
- [Sales handbook](/handbook/sales/)
- [Support handbook](/handbook/support/)
- [Workshops and Lunch-and-Learn slides](https://drive.google.com/drive/folders/1qAymFTiXFEk-lRSNreIhaZ6Z62fdo_y2)
