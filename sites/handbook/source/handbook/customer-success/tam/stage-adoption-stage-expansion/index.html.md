---
layout: handbook-page-toc
title: "Stage Adoption & Stage Expansion"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Stage Adoption & Stage Expansion

The TAM team's primary focus is to align with a customer's desired business outcomes, enable the customer in their current use case, and expand the customer's use case into new stages.

**Stage Adoption**
Stage adoption is the usage level of existing features and use cases within a given stage that a customer is using. As part of the TAM engagement, the TAM will work with the customer to provide guidance on adopting stages to maximize value attribution and align adoption to the customer's desired positive business outcomes. As stages include features across multiple subscription tiers, the TAM may drive stage adoption as part of proposing a subscription tier upgrade.**

**Stage Expansion**
Stage expansion is the process of helping a customer adoption a new stage, including the discovery of needs, value positioning, and enablement of the customer to adopt the new stage. The enablement can occur via digital content, TAM motion, and/or paid professional services.

### Stage Adoption:  Key TAM Strategies

1. Improve Onboarding, Reduce Time to Value
1. Product Intelligence led insights & digital enablement
1. Contribute to 'How To' in Gitlab Docs and Blogs
1. Enablement workshops & onboarding content, use case adoption teams

### Stage Expansion: Key TAM Strategies

1. Stage Expansion workshops and webinars
1. Effective trap-setting questions and playbooks based on proven paths to adoption
1. Metrics on time per stage, insights on adoption and non-successes
