---
layout: handbook-page-toc
title: "Enterprise Applications Architecture"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## High Level

### Systems Roadmap

- [Roadmap of Software](/handbook/business-ops/roadmap)

### Tech Stack/Software Applications

- [List of software applications and purpose](/handbook/business-ops/tech-stack/)
- [Tech Stack High Level](/handbook/business-ops/tech-stack-applications/)

### Baseline Entitlements

- [Baseline Entitlement High Level](/handbook/business-ops/employee-enablement/onboarding-access-requests/access-requests/baseline-entitlements/)

## Enterprise Application Ecosystem

### Finance Systems

- [Finance Architecture High Level](/handbook/business-ops/enterprise-applications/architecture/finance)

### Lead to Fulfillment

- [Business Fulfillment Documentation RoundUp](/handbook/business-ops/enterprise-applications/portal/)
- [Trade Compliance Operations](/handbook/business-ops/trade-compliance)
- [WIP: lead to fulfillment flow through systems and processes](https://app.lucidchart.com/documents/view/fe61ff48-c0e3-4f40-b2de-4023d48101d9)
- [System integrations and related emails](https://docs.google.com/spreadsheets/d/1j3xE6pQLfsKMri14LDcrnxbWbTwqz4Tpv9kI8UIHYCE/edit#gid=1849578778)

### People
