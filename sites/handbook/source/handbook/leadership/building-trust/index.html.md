---
layout: handbook-page-toc
title: Building Trust
layout: handbook-page-toc
title: "Building Trust at GitLab"
canonical_path: "/handbook/leadership/building-trust/"
description: "On this page, GitLab details considerations for building trust in remote teams. Learn more!"
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Trust is the cornerstone of how we operate at GitLab. We trust team members to [do the right thing instead of having rigid rules](https://about.gitlab.com/handbook/values/#measure-results-not-hours). Trust at GitLab [increases results](/handbook/people-group/guidance-on-feedback/), [efficiency](/handbook/values/#efficiency), and [collaboration](/handbook/values/#collaboration).

Trust takes time and energy to build. We leverage [informal communication](/company/culture/all-remote/informal-communication/) to build trust, but there are additional strategies [people leaders](/handbook/leadership/) and team members can implement on their teams. 

Working remotely can be isolating for team members if managers do not take the [necessary steps](/company/culture/all-remote/being-a-great-remote-manager/) to build trust and social cohesion amongst their teams. In a remote environment, it can take team members longer to get to know each other compared to a colocated office setting. On this page, we’ve outlined additional strategies people leaders and team members can apply to build trust amongst their teams. 

## Strategies to Build Trust 

People Leaders should make every effort to build trust as soon as they mobilize a team, when new team members start, and throughout the year. [Informal communication](/company/culture/all-remote/informal-communication/) is a great tool to build trust; however, there are additional strategies managers and team members can implement to build trust within the teams they support.

### Personality Assessments

Getting to know your people through personality assessments is a tool to build trust. Personality assessments can help teams understand how they are different and enable managers to better understand their direct reports. The results can shine a light on ways you can adapt your processes in a way that is most effective for different team members. Remember when we use personality assessments, it is essential to not type-cast team members into their assessed personality results. Instead use them as a tool to facilitate more meaningful conversations amongst your team. 

An abundance of free personality assessments are available. Several are outlined below:

- [Understanding Social Styles](/handbook/people-group/learning-and-development/emotional-intelligence/social-styles/#discover-your-social-style/)
- [Emotional Intelligence Assessments](/handbook/people-group/learning-and-development/emotional-intelligence/#emotional-intelligence-eq-assessments/)
- [Gallup Strengths Finder](https://www.gallup.com/cliftonstrengths/en/252137/home.aspx) 
- [16 Personalities](https://www.16personalities.com/) (also known as Myers-Briggs)
- [DISC Personality Test](https://discpersonalitytesting.com/free-disc-test/)
- [HEXACO Personality Inventory](https://hexaco.org/hexaco-online)

**How to apply personality assessments:** People leaders can have all team members take a personality assessment when they first join their team. Managers can examine personality assessment results and have strength-based discussions with their people. However, it is essential to never type-cast team members based on their results. Use personality assessments as a tool to facilitate trust and get to know each other. When we embrace our unique strengths and the unique strengths of our team members, we all feel more engaged, more productive, more valued, and more trusted. They should encourage their team members to share their strengths / styles with the team. In addition, team members can take personality assessment and update their READMEs accordingly with personality assessment information if they choose to.  

### Use a One-Slider Personal Summary

A one-slider personal summary is a tool for remote teams to get to know one another. Consider using the [template and example](https://docs.google.com/presentation/d/1N-fNiIBAc8_HefLyPTi4HEhoFkcCDLW_Ntx8wqmEQ9c/edit#slide=id.g822466666c_0_232) below as a starting point. The one-slider can be adapted to whatever questions or facts you'd like to learn more about. One-sliders allow team members to share fun facts about themselves in a remote setting. You can be creative and ask entertaining questions! When someone joins the team, have them fill out a one-slider and use it to introduce themselves to the team. They are fun, easy, and can help to build trust by getting to know one another in a remote setting. 

**How to apply one-sliders:** Before the next team call, ask one or two members of the team to pre-populate a one-slider a day or two in advance of the meeting. Spend the first five to ten minutes of the meeting having the designated team members share their one-slider. Give everyone an equal amount of time and encourage everyone on the call to ask questions about what makes them unique. 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTiYHr_SxtAHAcFr5Y5Lrnk39WMyNIRMqMiezKEHsG_dm_3Jd3a6jM1rDEycpCVVn5ZOZeySKHJeNQM/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

### Optional Weekly Team Social Call

Optional social calls are a strategy to increase trust. Anyone can organize one. They are tools to come together as a team informally. The secret is to make the meeting non-work related. Attendees are able to share photos related to the topic during their time speaking. Example optional social call topics:

- Top three songs of all time (you can play short snippets of a song) 
- Favorite foods
- Two truths and a lie
- If you won the lottery tomorrow, what would you do
- Would you rather
- What's the most exciting thing you've done outside of work this week/month
- Most recent or favorite vacation

**How to apply optional weekly team social calls:** Set a regular cadence of optional weekly team social calls (monthly, bi-weekly, weekly). Each social call, have one team member be in charge of managing and facilitating the discussion. They are responsible for proposing the topic and keeping the call moving. Remember to make these fun and try not to talk about work! 

To be inclusive of time zones and those who may not prefer these types of social settings, alternate synchronous social calls with [asynchronous](/company/culture/all-remote/asynchronous/) social standups. You can port the questions to the `Geekbot` Slack app so team members can share at their convenience.

### Zoom Background Ice-Breaker

The Zoom feature to upload [any background image](/handbook/tools-and-tips/zoom/#virtual-background) of your choice is a fun tool to use for an ice breaker. Managers can ask their team to come to a meeting with a background of their favorite place to vacation, or a place they hope to visit one day, and much more. It’s a great feature that can add some fun to calls and build unity. Spend the first five minutes of a meeting talking about the Zoom background and giving everyone in the meeting a voice. 

**How to apply Zoom background ice-breakers:** Team members can do this for as many meetings as they like. If it is a team discussion, spend the first five minutes of the meeting doing a round robin of the background. Managers can implement a rotation of DRIs ([directly responsible individuals](/handbook/people-group/directly-responsible-individuals/)) in charge of the ice-breaker by assigning different team members. 

### Give Credit to Team Members

Use the '#thanks' [Slack channel](/handbook/communication/#say-thanks/) to give recognition to team members in a public setting. Review team accomplishments weekly, monthly, and quarterly. Meet with each team member to review their accomplishments. 

Take a [coaching approach](/handbook/leadership/coaching/) and have the team members talk through what their accomplishments were. Managers should identify opportunities for [executives](/company/team/structure/#executives) to recognize the team. They should also look for opportunities to recognize the team in front of executives. 

**How to apply giving credit to team members:** Managers can set a weekly reminder to review team accomplishments and areas for individual recognition. Scheduling a regular cadence of giving credit will show team members that leaders are playing an active role in their career. 

### Leverage the Building High Performing Teams Model

The [Building High Performing Teams model](/handbook/people-group/learning-and-development/building-high-performing-teams/) is one of the most widely used and accepted models for team development. A big component of the model is building trust. To reach high performance, teams need to trust each other and share mutual regard where people are supported and respected. There is a willingness to be forthright, open, and free in working with team members. Review the [Trust section](/handbook/people-group/learning-and-development/building-high-performing-teams/#stage-2-trust-building---who-are-you/) of the Building High Performing teams page on strategies to build trust. 

**How to apply the building higher performing teams model:** Review the strategies to improve [building high performing teams](/handbook/people-group/learning-and-development/building-high-performing-teams/#summary/) at GitLab. In the [trust building section](/handbook/people-group/learning-and-development/building-high-performing-teams/#stage-2-trust-building---who-are-you/), Managers can conduct an assessment with their teams to see if team members trust one another. Use the resources outlined to facilitate a discussion. 

### Ask for Feedback

Managers should be open to [receiving feedback](/handbook/people-group/guidance-on-feedback/) from their team members. Often there is a barrier as employees feel uncomfortable sharing constructive feedback, let alone with their manager. But if managers are open to feedback, they can slowly grow a culture of feedback within their team, thereby increasing trust. Managers can show their openness by asking for feedback, through [1:1 calls](/handbook/leadership/1-1/) with direct reports or our performance management platform. 

**How to apply asking for feedback:** It may be difficult to ask for feedback from team members as a manager. However, people leaders should be in the habit of regularly asking for feedback. Every 1-1 can be an opportunity for team members to share feedback on your performance. Managers must establish an environment built on trust with [no ego](/handbook/values/#no-ego) to enable team members to be forthcoming. 

### Overcome Unconscious Bias

As people, we have a natural tendency to make assumptions about others, which means that once someone becomes a manager they might unintentionally favor some team members over others. [Unconscious bias](/company/culture/inclusion/unconscious-bias/) contributes to a major loss in trust between managers and employees.

**How to apply overcoming unconscious bias:** There are a host of free resources available to overcome unconscious bias. Managers and team members should spend time understanding their bias. Take the [implicit-association test (IAT)](https://implicit.harvard.edu/implicit/) as a first step. Block time on your calendar to take [Diversity, Inclusion, and Belonging](/company/culture/inclusion/) related training every quarter. Apply empathy to discussions amongst your team and put yourself in others shoes to better understand their perspective. 

### Take Time Out To Learn

Managers can make it clear that they have a vested stake in team members career development by advocating for their team to take time to learn. Team members look to their managers to provide learning guidance. As a People Leader, send your team interesting articles, thought leadership studies, videos, etc, on the latest trends in your domain. Managers should advocate to take time to learn for reskilling and upskilling on a range of topics relevant to the job. Managers can set an example by fostering a learning culture within the organization by sending a strong message that it is okay to take time out to learn.

**How to apply taking time out to learn:** People leaders can send a clear message to their teams by sending them weekly learning excrcises to break up the workweek. Try sending out interesting webinars and articles that are relevant to their job family. Encourage team members to take advantage of our tuition reimbursement policy to grow professionally. Managers can also encourage their team to block a portion of their week for focusing on professional development.  

### "No Work Talk" Team Chats

Managers can set up a more relaxed call with their team that is specifically focused on personally getting to know one another as individuals. It helps to remind us all that at the end of the day, we're people and we have unique hobbies, interests, and milestones happening in our lives! **The only rule for this call: no talking about work!** 

**How to apply "no work talk" team chats:** People leaders can set up a calendar invite (with all team members marked as optional) weekly or at a desired cadence. Encourage team members to join and get to know each other better in a remote environment. Come prepared with some possible ice breakers if the conversation doesn't immediately flow. An easy one: what are your plans for the weekend? Timezones can be tricky, so if you're team is represented globally and it's not possible to schedule a single time for all team members to join, it is recommended to rotate the time weekly so that everyone can be included.

## Learning Speaker Series - Building Trust with Remote Teams

On 2020-11-19, the L&D team launched our first [learning speaker series](/handbook/people-group/learning-and-development/learning-initiatives/#learning-speaker-series-overview) on Building Trust with Remote Teams. We hosted [Dr. Jeb Hurley](https://onehabit.blog/building-trust-remote-teams/), CEO and Co-Founder of [Xmetryx](https://www.xmetryx.com/) to discuss how strategies and tips on how to build trust in a remote setting. The discussion was a [fireside chat](https://docs.google.com/document/d/17ziw6q-nioyzYGnnIa9LM1G3I4DwYfb2UxwN9XceFxg/edit) format where Dr. Hurley shared what remote team members can do to meaure trust on their teams. 

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/hHMDY77upAE" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>


## Summary

Building trust takes time and energy. These are not the only strategies teams can apply to build trust but it is a start. Use this page as a guide to building a cohesive and collaborative team. Trust among teammates is the foundation of almost everything we do at GitLab. 

If you have further suggestions on building trust in a remote team, make a merge request to this page and alert our [Learning & Development team](/handbook/people-group/learning-and-development/#how-to-communicate-with-us).