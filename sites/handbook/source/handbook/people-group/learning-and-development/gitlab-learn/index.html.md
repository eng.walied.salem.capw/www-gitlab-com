---
layout: handbook-page-toc
title: GitLab Learn
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Learn

## What is EdCast?

GitLab uses EdCast, a Learning Experience Platform (LXP). Our EdCast instance is named GitLab Learn. At GitLab, we belive [everyone can contribute](https://about.gitlab.com/company/strategy/#mission), and our LXP is no different. [Everyone can contribute to GitLab Learn!](/handbook/people-group/learning-and-development/gitlab-learn/contribute/) 

You can read more about the mission and vision of the LXP on the [EdCast About Page](https://www.edcast.com/corp/about-us/)

Users of GitLab Learn can find support in our [user docs](/handbook/people-group/learning-and-development/gitlab-learn/user/) 

### What is a LXP?

A learning experience platform, or LXP, is [defined by EdCast](https://www.edcast.com/corp/blog/what-is-an-lxp/#:~:text=It%20brings%20together%20internal%2C%20external,the%20content%20to%20the%20user.) as a platform that "brings together internal, external, formal and informal sources of learning and knowledge in a simple, easy to use interface (UI), driving an intuitive user experience (Ux).  A key feature is that an LXP should use Artificial Intelligence and Machine Learning (AI/ML) to personalize the content to the user. An LXP should be available on the web, and on a mobile device, anytime, anywhere."

## GitLab's Long Term Strategic Vision for the LXP

Additional information about the long term strategie vision for GitLab Learn is coming soon.

#### A note about naming

Review the following terms to get familiar with language used in this documentation.

| Term | Definition |
| ----- | ----- |
| EdCast | The vendor we're collaborating with to create GitLab Learn. |
| GitLab Learn | GitLab's EdCast instance |
| Learning Experience Platform (LXP) | The type of learning platform that GitLab is using to organize learning content. Learn more in the [L&D handbook](https://about.gitlab.com/handbook/people-group/learning-and-development/#gitlab-learn-edcast-learning-experience-platform-lxp) |


### What will GitLab Learn do for the GitLab community

GitLab has invested in the LXP to enable our team members, partners, customers, and the broader community a personalized, self-service leaning platform to enable handbook first continuous learning. GitLab Learn will do the following for GitLab: 

- **Aggregate** all knowledge and learning content into one system for easier accessibilitiy 
- **Curate & Create** content that is [handbook first with interactivity](/handbook/people-group/learning-and-development/interactive-learning/)
- **Recommend** learning based on individual community member preferences
- **Learn & Guide** community members on how to develop new skills through step-by-step guides through individual learning styles
- **Automate** learning for personalized learning recommendations
- **Scale** learning to the wider GitLab community
- **On-Demand**, **Self-Paced** learning
- **Reduce** manual tasks associated with scaling
- **Enhance** training to be delivered in a fun and engaging way (erase your preconceived notions of LMS or LXP systems!)
- **Dogfood** the same LXP system that will be used for customer training offerings


## Handbook first approach to the LXP

The GitLab LXP uses a [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first) approach for all learning content, using the handbook as our single source of truth for learning content. Contributors to the LXP will use the platform to [find and curate](/handbook/people-group/learning-and-development/interactive-learning/) relevant content that exists in the handbook using a structured process. 


## Governance

Implementation of the EdCast LXP at GitLab requires cross-functional collaboration across organizations to serve various audience needs. In the spirit of driving clarity, visibility, and accountability, we've identified DRIs for the roles & responsibilities outlined below.

Overall LXP Implementation DRI: Josh Zimmerman Learning & Development

### Steering Committee

| Name | Team | Executive Sponsor |
| ------ | ------ | ------ |
| David Sommers | Field Enablement | David Hong |
| Christine Yoshida | Professional Services | Michael Lutz/David Sakamoto |
| Josh Zimmerman | L&D | Carol Teskey |
| Phil Encarnacion | IT/Enterprise Applications | Christopher Nelson |
| Ed Cepulis | Channel/Partner Enablement | Michelle Hodges | 
| Dan Gordon | Marketing | Todd Barr | 

### Audience Workstreams

- GitLab Team Members
- Partners
- Customers
- Community Members

### LXP Project DRIs

| Gap | Description | DRI |
| ------ | ------ | ------ |
| Exec alignment | Need to be 100% aligned with LXP vision with Sid and e-group | David Somers |
| Content QA - Handbook Alignment | Responsible for ensuring that all content on the LXP is handbook first, especially since content is regularly updated in the handbook and with the use of Articulate 360 (third party tool)  | Recommendation: Issac Abbasi |
| Content look and feel | Ensuring that all content has a similar look and feel, unified across Field Enablement, Partner Enablement, Community, Marketing, L&D, etc. | Kendra Marquart |
| Legal | Alignment with legal on EdCast features and capabilities for compliance of course content | Robert Nalen |
| Branding | Need to have an EdCast designer that develops the look and feel  that is the same for partners, customers, and team members. Need logo, name, SEO, landing page, card styles, css style files, etc. | TBD-Marketing |
| Marketing | Coordination with marketing team on larger marketing strategy and tactics (i.e. do we have campaigns, where is the landing page, what is the SEO, how does this fit into pipeline, third party content providers (Coursera)) | Samantha Leee  |
| Manager Functionality & Reporting | Need to determine how managers can monitor team member learning paths and dashboard of training completion | Learning and Development Team |
| System Design | Should the owner of systems settings be organized at the highest level, not by department? | TBD-Core team |
| Content Strategy | Need to determine what the content categories are across the organization | TBD-Core team |
| Content Management | Will content be managed by respective departments or will this be done a centralized level? | TBD-Core team |
| Assessments | Standardization of assessment look and feel across the organization? | TBD-Core team |
| Notifications | Determine how customers, partners, and team members will receive notifications and who owns the notification process to respective parties | TBD-Core team |
| Support | Determine how technical issues and questions about the LXP will be managed | TBD-Core team |
| Learn@GitLab Alignment | Determine how the LXP will be linked with the [Learn@GitLab site](https://gitlab.com/groups/gitlab-com/marketing/-/epics/954#note_429575616) | Dan Gordon |


### Core Team Members and Roles

| Name | Team at GitLab |
| ----- | ----- |
| Phil Encarnacion | IT|
| Christopher Nelson | IT |
| Josh Zimmerman | Learning and Development |
| Jacie Bandur | Learning and Development |
| Samantha Lee | Learning and Development |
| David Somers | Field Enablement |
| John Blevins | Field Enablement |
| Kris Reynolds | Field Enablement |
| Eric Brown | Field Enablement |
| Monica Jacobs | Field Enablement |
| Tanuja Paruchuri | Field Enablement |
| Kelley Shirazi | Field Enablement |
| Issac Abbasi | Field Enablement |
| Christina Hupy | Marketing Enablement |
| Christopher Wang | Marketing Enablement |
| Alvaro Warden | Partner Enablement |
| Christine Yoshida | Education Services |
| Laci 'Lah tsi' Videmsky | Professional Services |
| Kendra Marquart | Professional Services |
| Joe Brady | Professional Services |
| Ed Cepulis | Channel Enablement |
| Kim Jaeger | Channel Enablement |
| Honora Duncan | Channel Enablement |
| Evon Collett | Channel Enablement |
| Boughty Canton | Channel Enablement |
| Dan Gordon | Technical Marketing |
| Julia Lake | Security/Legal |
| Lynsey Sayers | Security/Legal |

