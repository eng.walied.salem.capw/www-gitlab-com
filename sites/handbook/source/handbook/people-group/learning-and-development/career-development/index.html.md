---
layout: handbook-page-toc
title: Career Development
description: "It is the lifelong process of managing learning, work, leisure and transitions in order to move toward a personally determined and evolving preferred future."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is Career Development

It is the lifelong process of managing learning, work, leisure and transitions in order to move toward a personally determined and evolving preferred future.

### Roles and Responsibilities

Team Member Owned
 - Take charge of your own development and career
 - Close the skill gap for current and future positions
 - Seize development and stretch opportunities
 - Remember there are no promises or guarantees of a promotion

Manager Facilitated
- Take time with team members to discuss their career aspirations
- Listen and provide feedback, ideas and contacts
- Make choices to support their development

GitLab Supported
- Communicate future direction and skills requirements
- Provide information and tools when applicable
- Communicate internal opportunities

70-20-10 Rule for Development
- 70% of your development should come from on-the-job and action learning.  This can include development experiences like managing a project, serving on a cross-functional team, taking on a new task, job shadowing, job rotation, etc.
- 20% of your development should come from interactions with others.  This includes having a mentor, being a mentor, coaching, participating in communities of practice, serving as a leader in your organization, etc.
- 10% of your development should come from training, including classes, seminars, webinars, podcasts, reading, conferences, etc. Here is an example of the Enablement stage's process for applying for the [professional development budget](/handbook/engineering/development/enablement/processes/budget_approval.html).

Additional Questions to Think About
- Do you have any overused strengths or underdeveloped skills that might cause your career to stall or derail?
- Considering feedback from others, are you perceived to have the skills required for the business needs of the future? If not, how could you shape that perception in a favorable direction?
- How can you leverage your current skills and talents for your future aspirations?
- What skills or talents are missing to qualify you for your future aspirations?
- Does your feedback from others tell you anything about how feasible your aspirations are?
- Do you currently have the skills and talents needed for the future business needs?  If not what can you do now to get ready?

### External Resources some with no cost

- [Stanford's Centre for Professional Development](http://scpd.stanford.edu/home)
- [Yale Open Courseware](https://oyc.yale.edu/)
- [MIT Open Courseware](https://ocw.mit.edu/index.htm)
- [Notre Dame Open Courseware](https://www.edx.org/school/notredamex)

- [WorkLife with Adam Grant Podcast](https://www.ted.com/series/worklife_with_adam_grant)

- [Dose of Leadership with Richard Rierson - Authentic & Courageous Leadership Development](https://www.stitcher.com/podcast/dose-of-leadership-podcast)

### Internal Resources

During our Career Development workshops at Contribute, May 2019 we took team members through tips to creating a clear growth (aka development) plan. Below are the resources from that session:

- [Career Development Workshop, slides](https://docs.google.com/presentation/d/1yY0ofMGgzN07ylTAnRP5geFnWcgUYkiVlcIyR54tpD0/edit#slide=id.g29a70c6c35_0_68)
- [Individual Growth Plan template](https://docs.google.com/document/d/1ZjdIuK5mNpljiHnFMK4dvqfTOzV9iSJj66OtoYbniFM/edit)
- [Tips for Creating Effective Growth Plans](https://docs.google.com/document/d/1O45gRkQqUa3dEgjJXGwdBE7iZbBI22EPC7zrkS3T4dM/edit)
- [Career Development Worksheet](https://docs.google.com/presentation/d/104AFLl-45WVHbFqQFpNL8Ad-B5_vdY39wPEEmQsEKYI/edit#slide=id.g556339813d_0_2)
- [Performance Review template](https://docs.google.com/document/d/1Qy6Uh6wL4twayv1642iLhD6UlePvRqPL8WJmYBXsAWM/edit)

### Internal Opportunities to expand exposure

There are various internal opportunities to expand a team member's exposure to multiple parts of the organization.  These include:

- Participation in a [Team Member Resource Group (TMRG)](https://about.gitlab.com/company/culture/inclusion/erg-guide/)
- Work on a [Working Group (WG)](https://about.gitlab.com/company/team/structure/working-groups/)
- Do an [apprenticeship](https://about.gitlab.com/handbook/engineering/career-development/#apprenticeship-for-learning) on another team
- Writing for the [GitLab Blog](https://about.gitlab.com/handbook/marketing/blog/unfiltered/)
- Apply to do the [CEO shadow program](https://about.gitlab.com/handbook/ceo/shadow/) for two weeks
- When appropriate and possible, attending some of the staff meetings of the team members manager's manager

### Career Development Conversation Acknowledgements

Career development is a key factor in team member engagement and role satisfaction. As part of the FY'20 GitLab annual [engagement survey results](/handbook/people-group/engagement/) it was clear that team members want to have meaningful conversations with their managers on an annual basis or even more frequently. Starting in FY'22 we will be tracking career development conversations via BambooHR. 

#### Cadence

This process is just an acknowledgement by the team member that they have had career conversations. Aligned with the Performance/Potential [matrix cadence](/handbook/people-group/performance-assessments-and-succession-planning/#regular-cadence), formal career conversations will ideally happen twice per year:

* Once in Q2
* Once in Q4

The bi-annual cadence is not required, but recommended. Some team members may prefer annual career conversations, other team members may prefer more informal check-ins quarterly. This is a personal decision that should be made between team member and manager. The minimal recommended cadence for career development conversations is annnually, the most frequency recommended cadence is quarterly. 

#### Process 

_Please note that while managers can facilitate career development conversations with team members and help guide growth plans by using our [internal resources](/handbook/people-group/learning-and-development/career-development/#internal-resources),  managers cannot complete the acknowledgement process for team members. The team member is responsible for the acknowledgement in BambooHR._

Starting FY'22 (exact date is TBD) team members will receive a notice via BambooHR to acknowledge that a career conversation has occurred. By signing the acknowledgement you are confirming that you have indeed had a career development conversation during the specified time period (Q2 or Q4 respectively).

*  This is not mandatory, however we highly encourage all team members to discuss their career goals with their manager.
*  There is not one right way to document a career development conversation. Some team members may use their 1:1 document to capture the conversation and actions, others may use the [tools provided above](/handbook/people-group/learning-and-development/career-development/#internal-resources) to help guide the conversation, or a team member may use their own personal template or process to have a career development conversation.
*  There is no timeline or deadline on this process, however, the goal is for all team members to acknowledge at least one career development conversation in FY'22. 
*  If you have not had a career conversation yet it is up to you to schedule a time to review and discuss with your manager.  
*  If you are new to GitLab the recommendation is that you start career conversations after your first 90 days.
*  Career conversations should not be confused with promotion conversations. Team members who do not want to increase their scope of work or be promoted, but are performing, should not feel pressured to move up or out. Career conversations can also focus on helping team members identify projects or other activities that keep the team member engaged and learning new skills. _Please note that development can also include lateral moves, or moving to another speciality within the same job family and job level._  

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xUNupnJyTNY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Career Mapping and Development
{:#career-mapping-and-development}

We have started this process at GitLab by defining Junior, Senior and Staff advancement levels. Career Mapping helps GitLab team-members to understand and develop the skills they need to achieve their goals, giving them clear criteria.
Mapping helps managers and leaders internally develop the skills and knowledge they need to achieve future business goals. The key to this is to identify the key skills, knowledge, and abilities needed to master each level. Another essential tool is a career development plan, here are some examples:

- [Career Plan](https://docs.google.com/document/d/1hJIzMnVhEz3X4k24oAwNnlgGhBeQ518Cps9kLVRRoWQ/edit)
- [Template  - Development Scorecard](https://docs.google.com/spreadsheets/d/1DBrukzzsV6InaCkZf8_ngLeTcLQ9uj6ynE93qLmHkQA/edit#gid=1677297587)
- [Career Plan Template](https://performancemanager.successfactors.com/doc/po/develop_employee/carsample.html)

Managers should discuss career development at least once a month at the [1:1](/handbook/leadership/1-1/) and then support their team members with creating a plan to help them achieve their career goals. If you would to know more about this please checkout the [career mapping course video](https://www.youtube.com/watch?v=YoZH5Hhygc4)

### The Relationship Between Learning and Development and Promotion

As is highlighted in our [Leadership](/handbook/leadership/1-1/#key-points) section, GitLab team members should not feel pressure to climb the proverbial ladder. We recognize that not everyone wants to advance or move to a new level, and that is supported. Developing one's skills and promotion at the company are not mutually exclusive.

It is perfectly acceptable to seek out learning and development opportunities — to sharpen one's understanding of a coding language to better understand a function, etc. — yet not strive for promotion beyond your current role. Some team members are happier and more productive without managing a team, for example.

As detailed in GitLab's [Definition of Diversity, Inclusion & Belonging ](/company/culture/inclusion/), we recognize that unique characteristics and experiences form how we as individuals approach challenges and solve problems. They also shape how we view success in our individual careers and lives. Not everyone views promotion as a measure of success, and team members will not be thought less of or penalized for holding this view.

As part of GitLab's [Transparency](/handbook/values/#transparency) value, team members are encouraged to be open and honest with their manager. You are encouraged to learn and develop your skills without pressure to in turn seek promotion. If you feel you are not being supported in this way, please visit the [Need Help?](/handbook/people-group/#reach-peopleops) portion of the People Group Handbook.

### Performance Enablement Review

Effective from FY21-Q2, on a quarterly basis, each member of the E-Group will have a performance conversation with their direct manager, the [CEO](/company/team/#sytses), at the beginning of every quarter. These performance conversations will enable the continued performance improvement and personal development for the E-Group member. These quarterly meetings may include their [Executive Stable Counterpart](/handbook/board-meetings/#executive-stable-counterparts) and their [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) to provide guidance and feedback as part of this conversation. 

Performance will be reviewed in three parts which are not all of equal weighting but together make up 100%:

- **Results** has a 40% weighting and is split out to be 20% for KPIs, 10% for OKR Achievement, and 10% for OKR Ambitiousness. By including both OKR Achievement and OKR Ambitiousness, we encourage ambitious OKRs while also measuring the performance of OKRs. The goal is to avoid the negative experiences associated with [MBOs](https://www.investopedia.com/terms/m/management-by-objectives.asp), such as sacrificing quality for quantity. 
- Each quarter a **personal development goal** will be agreed and will account for 20%
- Each quarter there will be a focus on **Engagement** and will account for 20%
- Each quarter our **Values** will account for 20%  
- While each of these factors will help determine the appropriate Performance Factor, manager discretion will also be taken into account

| Quarter | Values 20% | Results 40% | Professional Development 20% | Engagement 20% |      
|-----------------------------------|-----------------|------------------|-----------------|-----------------|
| Q1 (April) | Values | 20% KPI / 10% OKR Achievement / 10% OKR Ambition | Individual Growth Goal | Vol attrition and number of PDPs | 
| Q2 (June) | Values | 20% KPI / 10% OKR Achievement / 10% OKR Ambition | Individual Growth Goal | 360 Results | 
| Q3 (October) | Values | 20% KPI / 10% OKR Achievement / 10% OKR Ambition | Individual Growth Goal | DIB Score  |
| Q4 (January) | Values | 20% KPI / 10% OKR Achievement / 10% OKR Ambition | Individual Growth Goal | Engagement Score | 

**Performance Factors**

The output from these quarterly meetings will be one of the below performance factors

- Developing in role i.e. you are new to company / new to role  or there is performance development required
- Performing in role i.e. you are meeting all requirements
- Exceeding in role i.e. you are knocking it out of the park

**Compensation reviews**

When conducting compensation reviews for the E-Group, GitLab will review two items: 1) alignment to market rates 2) performance factors from each quarter.
