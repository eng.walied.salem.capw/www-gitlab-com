---
layout: solutions
title: Deploy to Google Cloud Platform
description: GKE integration for GitLab CI/CD spins up Kubernetes clusters for scalable app deployment on Google Cloud Platform.
canonical_path: "/solutions/google-cloud-platform/"
suppress_header: true
---

***
{:.header-start}

![GCP logo](/images/solutions/gcp/gcp-partner.png)
{:.header-right.header-right-30}

# GitLab on Google Cloud
{:.header-left.header-left-65}

A complete DevOps platform to build, test, secure and deploy on Google Cloud
{:.header-left.header-left-65}

[Talk to an expert](/sales/){: .btn .cta-btn .orange}
{:.header-left.header-left-65}

***
{:.header-end}

> GitLab is a single application for the complete DevOps lifecycle from project planning and source code management to CI/CD, monitoring and security. With a tight Google Kubernetes Engine (GKE) integration, GitLab accelerates your software development and delivery.

***

## Joint Google Cloud and GitLab Benefits

* Modernize the business with Cloud Native solutions.
    * Enabling seamless deployments to Google Kubernetes Engine (GKE), CloudRun, Compute Engine, and more from a single application.
* Enforce security by default across all operations.
    * Secure code before deploying to production with [GitLab Secure](/stages-devops-lifecycle/secure/) and keep code protected at scale on Google Cloud infrastructure.
* Standardize your development platform with GitLab and Anthos.
    * GitLab provides DevOps workflow flexibility on top of Anthos’ unified platform to manage hybrid / multi-cloud environments.
* Unified workflow and optimized efficiency.
    * Unlock concurrent development and automate workflows via CI/CD for reliable and consistent cloud operations. Enable all teams across the software development  lifecycle to work concurrently.
{:.list-benefits}

[Learn More](/resources/downloads/GitLab_GCP_Solution_Brief.pdf){: .btn .cta-btn .purple}

***

## Get up to $500 in free credit for Google Cloud Platform

Every new Google Cloud Platform account associated with your business email receives $300 in credit upon sign up. In partnership with Google, GitLab is able to offer an additional $200 for both new and existing GCP accounts to get started with GitLab’s GKE Integration.

[Redeem $200](https://cloud.google.com/partners/partnercredit?pcn_code=0014M00001h35gDQAQ?utm_campaign=2018_cpanel&utm_source=gitlab&utm_medium=referral){: .btn .cta-btn .purple}

***

![GCP logo](/images/solutions/gcp/works-with-anthos.png)
{:.image-left}

## GitLab and MultiCloud
{:.header-left}

***
{:.clear}

As more organizations look to modernize their business through software defined means, enabling DevOps teams becomes paramount to their success. DevOps is as cultural as it is technology focused. GitLab as a single DevOps application enables cross functional teams to concurrently operate together while lowering the barrier of entry into Cloud Native solutions like Kubernetes, [Knative](https://cloud.google.com/knative/), and Istio. For example, GitLab users can take advantage of built-in support for Knative middleware components, allowing teams to quickly run [serverless](/product/serverless/) workloads on top of Kubernetes or directly with CloudRun on Google Cloud within a single UI.

Overall, GitLab’s integrations with GCE, GKE, CloudRun, and Anthos is a great starting point for enterprises that are transitioning to the cloud with heterogeneous environments. GitLab and Google Cloud provide users with Multicloud, Hybrid Cloud, or all-in-the-cloud deployment strategies. With GitLab’s robust CI/CD capabilities, organizations can define their specific use case workflow across almost any use case and abstract operations through automation. To learn more, check out co-founder of the Kubernetes project, [Tim Hockin’s take](https://www.youtube.com/watch?v=nAn1_m8JOjs&list=PLFGfElNsQthaRSxKnPphH9k4iD7RzRLyc&index=20&t=0s), on why organizations are wanting or needing to use multiple clusters and multiple clouds.

***

## Joint Solution Capabilities with Google Cloud

* ![GKE logo](/images/solutions/gcp/gke.png) Google Kubernetes Engine (GKE)
    * [GKE](https://cloud.google.com/kubernetes-engine) is Google’s managed Kubernetes service, designed to automate deployment, scaling, and management of containerized Linux and [Windows](https://www.youtube.com/watch?v=DEy7SQY9Xlw) applications. With [GitLab’s GKE integration](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#creating-the-cluster-on-gke), teams can quickly provision new GKE clusters or import existing clusters in just a few clicks. Additionally, GitLab’s [AutoDevops](https://docs.gitlab.com/ee/topics/autodevops/) functionality provides DevOps teams the lowest barrier of entry to getting started with CI/CD and deploying container workloads to GKE. Get started by [installing GitLab on GKE](https://cloud.google.com/solutions/deploying-production-ready-gitlab-on-gke) today. **[[Learn More]](/blog/2020/03/27/gitlab-ci-on-google-kubernetes-engine/)**
* ![Anthos logo](/images/solutions/gcp/anthos.png) Anthos
    * Anthos is a modern application platform that provides consistent development and operations experience for cloud and on-prem environments. GitLab supports Anthos’ core components like GKE On Premise (GKE-OP), [CloudRun for Anthos](/blog/2019/11/19/gitlab-serverless-with-cloudrun-for-anthos/), and [Anthos Configuration Management](https://cloud.google.com/solutions/best-practices-for-policy-management-with-anthos-config-management) to provide development workflow consistency on top of Antho’s unified infrastructure management platform. GitLab [supports on-prem GKE for hybrid cloud customers](https://cloud.google.com/solutions/partners/deploying-gitlab-anthos-gke-on-prem-cicd-pipeline).Together, GitLab with Anthos provides enterprises consistency and scalability across heterogeneous environments. **[[Learn More]](https://www.youtube.com/watch?v=MOALiliVoeg)**
* ![Cloud Run logo](/images/solutions/gcp/cloud-run.png) Cloud Run 
    * Cloud Run is a fully managed serverless platform that automatically scales your stateless containers and abstracts away all infrastructure management. [GitLab Serverless](/stages-devops-lifecycle/serverless/) enables deploying to Cloud Run with a full CI/CD workflow to build and test serverless applications. With GitLab for Cloud Run, enable your teams to streamline and simplify serverless management on any infrastructure (Knative, Cloud Run, Cloud Run for Anthos, etc.) through a single UI. **[[Learn More]](https://docs.gitlab.com/ee/user/project/clusters/serverless/)**
* ![GCE logo](/images/solutions/gcp/gce.png) Google Compute Engine 
    * Google Compute Engine (GCE) delivers configurable, high performance virtual machines running in Google’s data centers. Gitlab CI/CD provides application delivery to virtual machines as deployment targets. GitLab enables migration of traditional workloads that are non containerized to the cloud. Get started by installing GitLab on a single GCE instance or in [High Availability architecture](https://docs.gitlab.com/ee/administration/reference_architectures/index.html). **[[Learn More]](https://docs.gitlab.com/ee/install/google_cloud_platform/)**
* ![GAE logo](/images/solutions/gcp/gae.png) Google App Engine
    * Google App Engine (GAE) is a Platform as a Service for hosting serverless and web applications in Google-managed data centers. With GitLab, teams can collaborate and automatically deploy containers via CI directly onto GAE. GitLab as a single DevOps application enables teams to build and deploy code to yet another serverless type platform. **[[Learn More]](https://medium.com/faun/deploy-directly-from-gitlab-to-google-app-engine-d78bc3f9c983)**
* ![GCF logo](/images/solutions/gcp/gcf.png) Google Cloud Functions
    * Google Cloud Functions (GCF) is Google Cloud’s event-driven serverless compute platform. Store your code in GitLab SCM and directly deploy as cloud functions through GitLab CI/CD. Empower your teams to adopt GCP for a more event driven, Cloud Native architecture with GitLab and GCF. For example, automating development for [Firebase and Cloud Functions](/blog/2020/03/16/gitlab-ci-cd-with-firebase/). **[[Learn More]](https://medium.com/faun/deploy-directly-from-gitlab-to-google-app-engine-d78bc3f9c983)**
* ![Firebase logo](/images/google-cloud-platform/google_firebase_image.png) Firebase
   * Firebase is a platform for creating mobile and web applications developed by Google. GitLab SCM and CI together can help developers automate with first-class CI/CD pipelines to build, test, and deploy updates frequently to the entire Firebase stack. Check out this [blog on how to leverage GitLab CI/CD for Google Firebase](/blog/2020/03/16/gitlab-ci-cd-with-firebase/). **[[Learn More]](hhttps://www.youtube.com/watch?v=zworLJhEggE&feature=youtu.be)**
{:.list-capabilities}

***

## GitLab + GCP Customer Success Stories
{:.no-color}

* [Prometheus, CoreDNS, and Kubernetes itself are built, deployed, and tested by CNCF using GitLab. They deploy multiple projects to multiple clouds with end-to-end testing using GitLab multi-project deploy boards to monitor progress.](/customers/cncf/)
* [The cloud providers take on Multicloud- MulticloudCon San Diego](https://www.youtube.com/watch?v=US-dhWiNzWQ&feature=youtu.be)
* [Introduction to GitLab on GKE](https://www.qwiklabs.com/focuses/4815?catalog_rank=%7B%22rank%22%3A1%2C%22num_filters%22%3A0%2C%22has_search%22%3Atrue%7D&parent=catalog&search_id=2843491)
* [Kubernetes and the future of cloud native: We chat with Kelsey Hightower](/blog/2019/05/13/kubernetes-chat-with-kelsey-hightower/)
* [Because of GitLab’s integrations with Knative, ANWB is exploring Google Cloud on Kubernetes.](/customers/anwb/)
{:.list-resources}



&nbsp;

> “We had developers that thought, Why would we do something else? Jenkins is fine. But I think those people need to see GitLab first and see what the difference is because GitLab is so much more than Jenkins. The power of GitLab is you can do so much more and you can make everything so much easier to manage.” -- Michiel Crefcoeur, Frontend Build and Release Engineer

![anwb logo](/images/solutions/gcp/anwb.png)
{:.width-150}

***
