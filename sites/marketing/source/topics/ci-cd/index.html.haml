---
layout: default
title: What is CI/CD? | GitLab
description: "Learn how GitLab’s continuous integration and continuous delivery (CI/CD)  process automates software development workflows, reduces costs and ensures code quality"
twitter_image: '/images/opengraph/ci-cd-opengraph.png'
suppress_header: true
extra_css:
  - devops.css
  - cicd.css
  - cta-promo.css
extra_js:
  - libs/on-scroll.js
  - all-clickable.js
canonical_path: "/topics/ci-cd/"
clusterType: ci
sidebar_nav: true
sidebar_nav_title: "Related articles"
sidebar_nav_links:
  - title: 'Continuous integration pipelines'
    url: '/topics/ci-cd/continuous-integration-pipeline/'
  - title: 'Benefits of continous integration'
    url: '/topics/ci-cd/benefits-continuous-integration/'
  - title: 'Implement continous integration'
    url: '/topics/ci-cd/implement-continuous-integration/'
  - title: 'Continuous integration best practices'
    url: '/topics/ci-cd/continuous-integration-best-practices/'
  - title: 'Pipeline as code'
    url: '/topics/ci-cd/pipeline-as-code/'
  - title: 'Shift left DevOps'
    url: '/topics/ci-cd/shift-left-devops/'
---
:erb
  <script type="application/ld+json">
  {
    "@context": "https://schema.org",
    "@type": "FAQPage",
    "mainEntity": [
      {
        "@type": "Question",
        "name": "What is CI/CD",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "<p>Continuous integration (CI) and continuous delivery (CD) enable DevOps teams to increase the speed of sofware development and deliver better quality code, faster. Continuous integration works to integrate code from your team in a shared repository vastly improving your deployment pipeline. Developers share their new code in a Merge (Pull) Request, which triggers a pipeline to build, test, and validate the new code before merging the changes in your repository. Continuous delivery deploys CI-validated code to your application.</p> <p><a href='https://about.gitlab.com/topics/ci-cd/#what-is-ci-cd'>Learn more about the CI/CD</a>.</p>"
        }
      },
      {
        "@type": "Question",
        "name": "Benefits of CI/CD",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "<p>CI/CD automates workflows and reduces error rates within a production environment, which can have far-reaching impacts on not just development teams but throughout a whole organization.</p> <ul><li>More time for innovation</li> <li>Better retention rates</li> <li>More revenue</li> <li>Business efficiency</li></ul> <p><a href='https://about.gitlab.com/topics/ci-cd/#benefits-of-ci-cd'>Learn more about the benefits of CI/CD</a>.</p>"
        }
      },
      {
        "@type": "Question",
        "name": "Why Gitlab CI/CD?",
        "acceptedAnswer": {
          "@type": "Answer",
          "text": "<p>GitLab is a single application for the entire DevOps lifecycle, meaning we fulfill all the fundamentals for CI/CD in one environment.</p><p><a href='https://about.gitlab.com/topics/ci-cd/#why-gitlab-ci-cd'>Learn more about GitLab CI/CD</a>.</p>"
        }
      }
    ]
  }
  </script>

.blank-header
  = image_tag "/images/home/icons-pattern-left.svg", class: "image-border image-border-left", alt: "Gitlab hero border pattern left svg"
  = image_tag "/images/home/icons-pattern-right.svg", class: "image-border image-border-right", alt: "Gitlab hero border pattern right svg"
  .header-content
    = image_tag "/images/cicd/g_gitlab-ci-cd.svg", class: "hero-image-huge", alt: "Gitlab devops loop svg"
    %h1 Continuous integration and delivery
    %p Learn more about continuous software development with GitLab CI/CD.
    = link_to "Watch a GitLab CI/CD webcast", "/webcast/mastering-ci-cd/", class: "btn cta-btn orange devops-cta"

.container.wrapper.wrapper--large-fonts
  #content.devops-content.u-margin-top-md.row
    - localvar_sidebar_enabled = current_page.data.sidebar_nav || false
    - if localvar_sidebar_enabled == true
      .col-md-2#sidebar-nav
        = partial "/includes/components/sidebar-nav"
    - if localvar_sidebar_enabled == true
      - localvar_sidebar_class = 'col-md-10'
    - else
      - localvar_sidebar_class = 'col-md-12'
    %div{class: "#{localvar_sidebar_class}"}
      .wrapper.js-in-page-nav-group{role: "main"}
        .row.u-margin-top-lg.js-in-page-nav-section#what-is-ci-cd
          .col-md-12
            %h2.u-text-brand What is CI/CD?

            %p Continuous integration (CI) and continuous delivery (CD) enable DevOps teams to increase the speed of sofware development and deliver better quality code, faster. Continuous integration works to integrate code from your team in a shared repository vastly improving your deployment pipeline. Developers that employ full CI CD share their new code in a Merge (Pull) Request, which triggers a pipeline to build, test, and validate the new code before merging the changes in your repository. Continuous delivery deploys CI-validated code to your application.

            %p All code is tested throughout each stage, ensuring better quality builds and applications with fewer bugs. CI/CD pipelines can determine what happens when builds pass or fail these tests, meaning that errors are identified much faster. As code goes through each stage of the development process, it&rsquo;s continually validated against many other changes in the repository happening concurrently, which ensures code integrity throughout the pipeline. Together, CI and CD accelerate how quickly your team delivers results for your customers and stakeholders.

            %blockquote.blockquote-alt.cicd-blockquote
              %b Continuous Integration
              is the practice of integrating code into a shared repository and building/testing each change automatically, as early as possible &ndash; usually several times a day.

            %blockquote.blockquote-alt.cicd-blockquote
              %b Continuous Delivery
              ensures CI-validated code can be released to production at any time.

            %p Continuous delivery is often used interchangeably with a continuous deployment release process, but there is a subtle difference between the two. Continuous deployment means that all validated code deploys to production automatically, whereas continuous delivery means that this code *can* be deployed. The flexibility for code to be deployed at any time is what differentiates delivery from deployment, and practicing continuous deployment is possible when continuous delivery is already in place.

        .row.u-margin-top-xl.js-in-page-nav-section#benefits-of-ci-cd
          .col-md-12
            %h2.u-text-brand How does CI/CD help DevOps?

            %p CI/CD automates workflows and reduces error rates within a production environment, which can have far-reaching impacts on not just development teams but throughout a whole organization.

            %ul
              %li More time for innovation
              %li Better retention rates
              %li More revenue
              %li Business efficiency

            %h3.bold-links
            = link_to "Learn more about the benefits of continuous integration →", "/topics/ci-cd/benefits-continuous-integration/"


        .row.u-margin-top-xl.js-in-page-nav-section#ci-cd-fundamentals
          .col-md-12
            %h2.u-text-brand Fundamentals of CI/CD

            %p CI/CD is all about efficiency and is built around fundamental elements to make it effective.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px" }
                  = image_tag "/images/icons/first-look-influence.svg"

              .media-body.media-middle
                %h3.u-margin-top-0 A single source repository

                %p.u-margin-bottom-0 Source code management (SCM) that houses all necessary files and scripts to create builds.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/build.svg"
              .media-body.media-middle
                %h3.u-margin-top-0 Automated builds

                %p.u-margin-bottom-0 Scripts should include everything you need to build from a single command.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/computer-test.svg"
              .media-body.media-middle
                %h3.u-margin-top-0 Builds should be self-testing

                %p.u-margin-bottom-0 Testing scripts should ensure that the failure of a test should result in a failed build.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/scale.svg", style: "height: 80px;"
              .media-body.media-middle
                %h3.u-margin-top-0 Frequent iterations

                %p.u-margin-bottom-0 Multiple commits to the repository mean there are fewer places for conflicts to hide.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/stable-computer.svg"
              .media-body.media-middle
                %h3.u-margin-top-0 Stable testing environments

                %p.u-margin-bottom-0 Code should be tested in a cloned version of the production environment.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/visibility.svg", style: "height: 80px;"
              .media-body.media-middle
                %h3.u-margin-top-0 Maximum visibility

                %p.u-margin-bottom-0 Every developer should be able to access the latest executables and see any changes made to the repository.

            .media.u-margin-top-sm.u-margin-bottom-md.has-shadowed-svg
              .media-left.media-middle.u-padding-right-sm
                .media-object{ style: "width: 80px; height: 80px;" }
                  = image_tag "/images/icons/gitlab-rocket.svg"
              .media-body.media-middle
                %h3.u-margin-top-0 Automated deployments

                %p.u-margin-bottom-0 Code should be able to deploy into multiple environments easily.

        .row.u-margin-top-md.js-in-page-nav-section#why-gitlab-ci-cd
          .col-md-12
            %h2.u-text-brand Why Gitlab CI/CD?

            %p In order to complete all the required fundamentals of full CI/CD, many CI platforms rely on integrations with other tools to fulfill those needs. Many organizations have to maintain costly and complicated toolchains in order to have full CI/CD capabilities. This often means maintaining a separate SCM like Bitbucket or GitHub, connecting to a separate testing tool, that connects to their CI tool, that connects to a deployment tool like Chef or Puppet, that also connects to various security and monitoring tools.

            %p Instead of just focusing on building great software, organizations have to also maintain and manage a complicated toolchain. GitLab is a single application for the entire DevOps lifecycle, meaning we fulfill all the fundamentals for CI/CD in one environment.

            %div.u-image-bg.u-text-light.u-margin-top-xl.u-margin-bottom-md{ style: "background-image:url('/images/scaling-devops-bg.jpg');" }
              .row.u-margin-top-lg.u-margin-bottom-lg{ style: "padding: 40px;" }
                .col-md-12
                  %h2 GitLab CI/CD rated #1

                  %h3.u-margin-top-md Build, test, deploy, and monitor your code from a single application.
                  %p We believe a single application that offers visibility across the entire SDLC is the best way to ensure that every development stage is included and optimized. When everything is under one roof, it’s easy to pinpoint workflow bottlenecks and evaluate the impact each element has on deployment speed. GitLab has CI/CD built right in, no plugins required.
                  <br>
                  %h5
                    = link_to "Explore GitLab CI", "/stages-devops-lifecycle/continuous-integration/", class: "btn cta-btn orange devops-cta"
                  %h5
                    = link_to "Explore GitLab CD", "/stages-devops-lifecycle/continuous-delivery/", class: "btn cta-btn orange devops-cta"


        .row.u-margin-top-xl.js-in-page-nav-section#suggested-reading
          .col-md-12
            %h2.u-text-brand Suggested reading

            .feature-group.feature-group--alt.u-margin-top-sm.u-padding-top-0.u-padding-bottom-0
              .row.flex-row
                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-gitlab-cicd.png", alt: 'DevOps', srcset: "/images/feature-thumbs/feature-thumb-gitlab-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title Why Gitlab CI/CD
                      %p.feature-description With GitLab’s out-of-the-box CI/CD, you can spend less time maintaining and more time creating.
                      = link_to "Read", "/blog/2019/04/02/why-gitlab-ci-cd/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-beginners-guide-cicd.png", alt: 'Create a CI/CD pipeline blog post', srcset: "/images/feature-thumbs/feature-thumb-beginners-guide-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title A beginner's guide to continuous integration
                      %p.feature-description Here's how to help everyone on your team, like designers and testers, get started with GitLab CI.
                      = link_to "Read", "/blog/2018/01/22/a-beginners-guide-to-continuous-integration/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-gitlab-logo-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-gitlab-logo-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title 5 teams that made the switch to GitLab
                      %p.feature-description See what happened when these five teams moved on from old continuous integration and delivery solutions and switched to GitLab CI/CD.
                      = link_to "Read", "/blog/2019/04/25/5-teams-that-made-the-switch-to-gitlab-ci-cd/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-why-gitlab-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-why-gitlab-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title 4 benefits of CI/CD
                      %p.feature-description How to measure a successful CI/CD strategy.
                      = link_to "Read", "/blog/2019/06/27/positive-outcomes-ci-cd/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-4-benefits-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-4-benefits-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title The business impact of CI/CD
                      %p.feature-description How a good CI/CD strategy generates revenue and keeps developers happy.
                      = link_to "Read", "/blog/2019/06/21/business-impact-ci-cd/", class: "feature-more"

                .col-md-4.col-lg-4.u-margin-bottom-sm
                  .feature.js-all-clickable
                    .feature-media
                      = image_tag "/images/feature-thumbs/feature-thumb-frontend-cicd.png", alt: 'GitLab & Kubernetes', srcset: "/images/feature-thumbs/feature-thumb-frontend-cicd_2x.png 2x"
                    .feature-body
                      %h3.feature-title How DevOps and GitLab CI/CD enhance a frontend workflow
                      %p.feature-description The GitLab frontend team uses DevOps and CI/CD to ensure code consistency, fast delivery, and simple automation.
                      = link_to "Read", "/blog/2018/08/09/how-devops-and-gitlab-cicd-enhance-a-frontend-workflow/", class: "feature-more"


        .row.u-margin-top-xl.js-in-page-nav-section#resources
          .col-md-12
            %h2.u-text-brand Resources


            %p Here’s a list of resources on CI/CD that we find to be particularly helpful in understanding CI/CD and implementation. We would love to get your recommendations on books, blogs, videos, podcasts and other resources that tell a great CI/CD story or offer valuable insight on the definition or implementation of the practice.

            %p
              Please share your favorites with us by tweeting us
              %a{:href => 'https://twitter.com/gitlab'} @GitLab!

            %hr.u-margin-top-sm.u-margin-bottom-sm

            .resource-block--webcasts
              %h3.block-title Webcasts
              %h5
                = link_to "Mastering continuous software development", "/webcast/mastering-ci-cd/"


            %hr.u-margin-top-sm.u-margin-bottom-sm


            .resource-block--blogs
              %h3.block-title Whitepapers and ebooks
              %h5
                = link_to "Scaled continuous integration and delivery", "/resources/scaled-ci-cd/"
              %h5
                = link_to "The benefits of single application CI/CD", "/resources/ebook-single-app-cicd/"


            %hr.u-margin-top-sm.u-margin-bottom-sm


            .resource-block--podcasts
              %h3.block-title Case studies
              %h5
                = link_to "Cloud Native Computing Foundation (CNCF)", "/customers/cncf/"
              %h5
                = link_to "Ticketmaster", "/blog/2017/06/07/continous-integration-ticketmaster/"
              %h5
                = link_to "Verizon", "/blog/2019/02/14/verizon-customer-story/"


            %hr.u-margin-top-sm.u-margin-bottom-sm


            .resource-block--podcasts
              %h3.block-title Reports
              %h5
                = link_to "Voted a leader in The 2019 Forrester Wave<sup>TM</sup>: Cloud-Native Continuous Integration Tools", "/resources/forrester-wave-cloudnative-ci/"
              %h5
                = link_to "Voted as a Strong Performer in The Forrester Wave™: Continuous Delivery And Release Automation, Q2 2020","/analysts/forrester-cdra20/"


        = partial "/includes/ctas/cta-promo"
