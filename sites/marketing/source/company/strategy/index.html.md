---
layout: markdown_page
title: "GitLab Strategy"
description: "GitLab believe that all digital products should be open to contributions; from legal documents to movie scripts, and from websites to chip designs."
canonical_path: "/company/strategy/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organization

GitLab's mission, vision, strategy, and planning follow a [cadence](/handbook/ceo/cadence).
Along each time period, we answer various fundamental questions: why, what, how,
and when. The matrix of questions vs time period is below, with mappings to the
appropriate sections of this page.

| [Time Period](/handbook/ceo/cadence/) | 30 Years | 10 Years | 3 Years | 1 Year |
|--------------------------------------:|:--------:|:--------:|:-------:|:------:|
| Why | [Why](#why) |  |  |  |
| What | [Mission](#mission), [BHAG](#big-hairy-audacious-goal) | [Vision](#vision),  [Goals](#goals) | [Strategy](#three-year-strategy) |  |
| How | [Values](#values) | [Everyone can contribute](#everyone-can-contribute), [Customer Acceptance](#customer-acceptance), [Monitoring an evolving market](#monitoring-an-evolving-market),  [Risks](#risks) | [Principles](#principles), [Assumptions](#assumptions), [Pricing](#pricing), [Dual Flywheels](#dual-flywheels), [KPIs](#publicly-viewable-okrs-and-kpis) |  |
| When |  |  | [Sequence](#sequence) | [Plan](#plan) |

## Why

We believe in a world where **everyone can contribute**. We believe that all
digital products should be open to contributions; from legal documents to movie
scripts, and from websites to chip designs.

Allowing everyone to make a proposal is the core of what a DVCS
([Distributed Version Control System](https://en.wikipedia.org/wiki/Distributed_version_control))
such as Git enables. No invite needed: if you can see it, you can contribute.

We think that it is logical that our collaboration tools are a collaborative
work themselves. More than [3,000 people from the wider community](http://contributors.gitlab.com/) have
contributed to GitLab to make that a reality.

## Mission

It is GitLab's mission to change all creative work from read-only to
read-write so that **everyone can contribute**.

When **everyone can contribute**, consumers become contributors and we greatly
increase the rate of human progress.

## Big Hairy Audacious Goal

Our [BHAG](https://www.jimcollins.com/concepts/bhag.html) over
[the next 30 years](/handbook/ceo/cadence/#mission)
is to become
the most popular collaboration tool for knowledge workers in any industry. For
this, we need to make the DevOps lifecycle much more user friendly.

## Values

Our mission guides our path, and we live our [values](/handbook/values/) along this path.

## Vision

In summary, our vision is as follows:

GitLab Inc. develops great open source software to enable people to collaborate
in this way. GitLab is a [single application](/handbook/product/single-application/)
based on
[convention over configuration](/handbook/product/product-principles/#convention-over-configuration)
that everyone should be able to afford and adapt. With GitLab, **everyone can
contribute**.

## Goals

1. Ensure that **everyone can contribute** in the three ways outlined above.
2. Become the most used software for the software development lifecycle and collaboration on all digital content by following [the sequence below](#sequence).
3. Complete our [product vision](/direction/#vision) of a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/product-principles/#convention-over-configuration).
4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).
5. Remain independent so we can preserve our values. Since we took external investment, we need a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event). To remain independent, we want to become a [public company](/handbook/being-a-public-company/) instead of being acquired.

##  Everyone can contribute

Everyone can contribute to digital products with GitLab, to GitLab itself, and to our organization.
There are three ways you can Contribute,
1.   [Everyone can contribute with GitLab](/company/strategy/#contribute-with-gitlab)
1.   [Everyone can contribute to GitLab, the application](/company/strategy/#contribute-to-gitlab-application)
1.   [Everyone can contribute to GitLab, the company](/company/strategy/#contribute-to-gitlab-company)

### Everyone can contribute with GitLab
{:#contribute-with-gitlab}

To ensure that **everyone can contribute with GitLab** we allow anyone to create a proposal, at any time, without setup, and with confidence. Let's analyze that sentence a bit.
   - Anyone: Every person in the world should be able to afford great DevOps software. GitLab.com has free private repos and CI runners and GitLab CE is [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). But open source is more than a license, that is why we are [a good steward of GitLab CE](/company/stewardship/) and keep both GitLab CE and EE open to inspection, modifications, enhancements, and suggestions.
   - Create: It is a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/product-principles/#convention-over-configuration).
   - Proposal: With Git, if you can read it, you can fork it to create a proposal.
   - At any time: You can work concurrently with other people, without having to wait for permission or approval from others.
   - Without setup: You can make something without installing or configuring for hours with our web IDE and Auto DevOps.
   - With confidence: Reduce the risk of a flawed proposal with review apps, CI/CD, code quality, security scans, performance testing, and monitoring.

### Everyone can contribute to GitLab, the application
{:#contribute-to-gitlab-application}

We actively welcome contributors to ensure that **everyone can contribute to GitLab, the application**.
We do this by having quality code, tests, documentation, popular frameworks, 
and offering a comprehensive [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
and a dedicated [GitLab Design System](https://design.gitlab.com/). 
We use GitLab at GitLab Inc., we [dogfood](/handbook/product/product-processes/#dogfood-everything) 
it and make it a tool we continue to love. We celebrate contributions by
recognizing a Most Valuable Person (MVP) every month. 
We allow everyone to anticipate, propose, discuss, and contribute features by having everything on
a public issue tracker. We ship a new version every month so contributions
and feedback are visible fast. To contribute to open source software, people
must be empowered to learn programming.
That is why we sponsor initiatives such as Rails Girls.
   There are a few significant, but often overlooked, nuances of the **everyone can contribute to GitLab, the application** mantra:

   * While collaboration is a core value of GitLab, over collaborating tends to involve team members unnecessarily, leading to consensus-based decision making, and ultimately slowing the pace of improvement in the GitLab application. Consider [doing it yourself](/handbook/values/#collaboration), creating a merge request, and facilitating a discussion on the solution.
   * For valuable features in line with our product philosophy, that do not yet exist within the application, don't worry about UX having a world class design before shipping. While we must be good stewards of maintaining a quality product, we also believe in rapid iteration to add polish and depth after an [MVC](/handbook/product/product-principles/#the-minimal-viable-change-mvc) is created.
   * Prefer creating merge requests ahead of issues in order to suggest a tangible change to facilitate collaboration, driving conversation to the recommended implementation.
   * Contributors should feel free to create what they need in GitLab. If quality engineering requires charting features, for example, which would normally be implemented out of another team, they should feel empowered to prioritize their own time to focus on this aspect of the application.
   * GitLab maintainers, developers, and Product Managers should be viewed as coaches for contributions, independent of source. While there are contributions that may not get merged as-is (such as copy/paste of EE code into the CE code base or features that aren't aligned with product philosophy), the goal is to coach contributors to contribute in ways that are cohesive to the rest of the application.

   A group discussion reiterating the importance of everyone being able to contribute:
   <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/l374J98iOmk?t=675" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
   </figure>

### Everyone can contribute to GitLab, the company
{:#contribute-to-gitlab-company}

To ensure that **everyone can contribute to GitLab, the company** we have open business processes.
This allows all team members to suggest improvements to our
handbook. We hire remotely so everyone with an internet connection can come
work for us and be judged on results, not presence in an office. We offer
equal opportunity for every nationality. We are agnostic to location and
create more equality of opportunity in the world. We engage on Hacker News,
Twitter, and our blog post comments. And we strive to take decisions guided
by [our values](/handbook/values/).

#### Everyone can contribute to about.gitlab.com

We welcome all contibutors in the [www-gitlab-com project](https://gitlab.com/gitlab-com/www-gitlab-com) so that **everyone can contribute to about.gitlab.com**. GitLab uses about.gitlab.com to share our expertise with the world and believe we can build even greater levels of trust with contributions from our team and community. We strive to provide a great experience for our existing and new community members by reviewing changes and integrating the contributions into our regularly planned updates.

### Customer acceptance

We firmly adhere to laws [including trade compliance laws](/handbook/people-group/code-of-conduct/#trade-compliance-exportimport-control) in countries where we do business, and welcome everyone abiding by those legal restrictions to be customers of GitLab. In some circumstances, we may opt to not work with particular organizations, on a case-by-case basis. Some reasons we may choose not to work with certain entities include, but are not limited to:

1. Engaging in illegal, unlawful behavior.
1. Making derogatory statements or threats toward anyone in our community.
1. Encouraging violence or discrimination against legally protected groups.

This policy is in alignment with our mission, contributor and employee code-of-conduct and company values. Here are some links that may give you some background at how we arrived at this customer acceptance policy:

* Our mission is "everyone can contribute." This mission is in alignment with our open source roots and the [MIT license](https://en.wikipedia.org/wiki/MIT_License) our open source software is subject to. The MIT License is a free software license that allows users the freedom to run the program as they wish, for any purpose.

* GitLab has a [contributor code of conduct](/community/contribute/code-of-conduct/) for _how_ to contribute to GitLab, but there are no restrictions on _who_ can contribute to GitLab. We desire that everyone can contribute, as long as they abide by the code of conduct.

* GitLab has a set of values for how Gitlabbers strive to conduct themselves. We don’t expect all companies to value collaboration, results, efficiency, diversity, inclusion and transparency in the same way we do. As an open company, “everyone can contribute” is our default and [transparency](/handbook/values/#transparency) is our check and balance. Transparency means our handbook, issues, merge requests and product roadmap are online for everyone to see and contribute to.

* Related topic: At GitLab, we want to avoid an environment where people feel alienated for their religious or political opinions. Therefore, we encourage Gitlabbers to refrain from taking positions on specific [religious or political issues](/handbook/values/#religion-and-politics-at-work) in public company forums (such as on the GitLab Contribute stage or Slack channels) because it is easy to alienate people that may have a minority opinion. It is acceptable to bring up these topics in social contexts such as coffee chats and real-life meetups with other coworkers, but always be aware of cultural sensitivities, exercise your best judgement, and make sure you stay within the boundaries of our  [Code of Business Conduct & Ethics](/handbook/people-group/code-of-conduct/). We always encourage [discussion and iteration](/handbook/values/#anyone-and-anything-can-be-questioned) on any company policy, including this one.

## Monitoring an evolving market

We'll also need to adapt with a changing market so that we meet customer needs. Netflix is a great example of this. Everyone knew that video on demand was the future. Netflix, however, started shipping DVDs over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.

Additionally, we need to ensure that our Platform is open. If a new, better version control technology enters the market, we will need to integrate it into our platform, as it is one component in an integrated DevOps product.

### Entering new markets

[GitLab](https://about.gitlab.com) has taken existing, fragmented software tooling markets, and by offering a consolidated offering instead, have created a new [blue ocean](https://www.blueoceanstrategy.com/what-is-blue-ocean-strategy/).

We would like to find more markets where we can repeat the same model.

The desirable characteristics of such markets fall into two stages: category consolidation and creation.  They are:

#### Category Consolidation

1. A set of customer needs that are currently served by multiple, independent software tools
1. Those tools may already integrate with each other or have the possibility of integration
1. Those tools operate in categories that are typically considered discreetly (e.g. with GitLab, SCM was one market, CI another)
1. There is no current 'winner' at consolidating this market, even if there are some attempts to combine some of the tool categories within said market
1. The users of the product would also be able to contribute to it e.g. with GitLab, the users are software developers, who can directly contribute code back to the product itself
1. When initially combined the categories form a consistent and desirable user flow which solves an overriding customer requirement
1. We can offer a consolidated toolchain more cost effectively for customers, than needing to purchase, run and integrate each tool separately
1. We can do so profitably for the company

#### Category Creation

1. By combining these categories together, a new overriding category, or market, gets created - the consolidated toolchain;
1. Further adjacent categories and/or markets can be added to deliver additional user value.  For example with GitLab, you could argue that the [Protect Category](/stages-devops-lifecycle/protect/) (as of October 2019) is an adjacent category/market to be added;
1. The sum of the categories combined should have desirable [CAGR](https://investinganswers.com/dictionary/c/compound-annual-growth-rate-cagr) such that entering the markets will mean entering those on a growth curve;
1. Not all of the individual categories need to be on the upward curve of the [technology adoption lifecycle](https://medium.com/@shivayogiks/what-is-technology-adoption-life-cycle-and-chasm-e07084e7991f) (TAL) - it is however necessary that there are enough future categories with high CAGR and early on in their adoption lifecycle - see example (very rough) diagram outlining this below:
(insert overlapping TALs);
1. The ideal overlap of the TALs is that the peaks of the curves are as close to each other as possible, so that when one TAL moves to Late Majority, the next TAL is still curving upwards.  This allows it to provide an organic growth opportunity for the company in the markets it is entering.

Our goal is to develop this model to be more quantifiable and formulaic, so that we can quickly and easily assess new opportunities.

### Risks

We acknowledge the risks to achieving our goals. We document them in our [biggest risks page](/handbook/leadership/biggest-risks/).

## Three Year Strategy

This is in draft until the end of 2020. We are gathering feedback in individual MRs instead of MR comments to keep it manageable.

Along the road to realizing our mission of **everyone can contribute**, our
strategic goal is to become the leading complete DevOps
platform (or, what Gartner calls the DevOps Value Stream Delivery platform) delivered as a [single application](/handbook/product/single-application/).  We will help our customers build better software faster including developing cloud native applications, building mobile applications, and incorporating machine learning and artificial intelligence into their products and solutions. We believe that our [dual flywheels](#dual-flywheels) (single application + open-core model) will propel us to efficiently be the default choice  in the Enterprise market.

To achieve this, we will endeavor to: 

### 1) Accelerate market maturity around the DevOps Platform 
Ensure that when customers buy DevOps software the single application approach is the default. 

1. **Deliver products** that are mature and enterprise ready across a broad range of categories ([50%](/company/strategy/2023/) of our [categories](/handbook/product/categories/) are at [lovable maturity](/direction/maturity)).
1. **Drive product adoption** by focusing on usability for new customers and cross promoting stages, we hope to achieve 100m estimated [Total Monthly Active Users](/handbook/product/performance-indicators/#estimated-total-monthly-active-users-tmau)
1. **Be a thought leader:** We aspire to become the trusted voice about the future of the DevOps industry by being the number one IT vendor ranking in Google searches for DevOps and DevOps Platform
1. **Create a strong partner ecosystem:**  Do our best to develop alliances, systems integrator and channel partners to service specific markets to drive [partner initiatied opportunities](/handbook/sales/field-operations/channel-operations/#partner-initiated-opportunity---pio) to contribute 30% of incremental ARR and 20% of new logos.

### 2) Build on our Open Core strength: 

We will strive to increase the contributions of the wider community.

1. **Community contributions:** Build out a community engagement program to drive community contributions per month from our open source community to 50% of the wider community by EOFY23.
1. **Customer contributions:** We will seek to encourage existing customers to make GitLab work for them with a [MRARR](/handbook/engineering/performance-indicators/#mrarr) above $1b MR per year.
1. **Enterprise leading usability:** Despite having many contributions improve the user experience of our application through purpose driven user research and product development leading to a SUS score > 80.
1. **Connect with core users:** Know the organization and be in contact with at least one person in organizations that collectively host more than 10m active users of our open source product. Do this via SaaS services for self-managed and proprietary features that can be used for free.

### 3) Customer Centricity: 
The customer will be the center of our operations including how we develop, market, and support our products. While we ship fast every month, we are committed to a seamless change management experience for our customers so they can get the best of GitLab in a predictable way.  We have a goal that over one thousand of our customers become references.

1. **Land and expand:** Acquire high-potential new customers for an initial purchase sized to land quickly. We will help the customer realize value and ROI from GitLab through product experience, customer success and channel services.  Through guided realization of value, we believe customers will choose to expand paid usage of additional users, higher tiers, and adopt additional features and DevOps lifecycle stages. Y% of GitLab customers consume strategy, management, implementation, and training services. Maintain a net retention rate of over X%.
1. **Use case driven conversion:** Use the product and our customer-facing teams to be empathetic and drive conversion through the customer journey to achieve a growth efficiency above 1.0:
Focus efforts in the hope of increasing free to paid for SaaS to 2%, self managed Core to paid at X per month while generating Y product qualified leads per month. Add the next whole complete product capability in the customer journey each year to prompt customers to upgrade at a rate of X% per year.  
1. **Strong data insights:** Invest in our data infrastructure to understand how 100% of our paid customers utilize GitLab so we can best prioritize and improve our products and enhance the customer experience.  
1. **Best in class commerce experience:** Ensure that it is easy for customers to try GitLab and do business with us whether they buy through our website, sales team or channel ecosystem with an industry-leading commercial systems capability. Today we sell Gitlab by the seat and we aim to expand our product offering into providing SaaS services for self-managed customers. 100% of our customers transact through this system with less than 5% customer support rate.

### 4) Inclusive growth & development for team members: 
Developing our team members is a core operating principle at GitLab.  We offer a competitive, performance-based total rewards package, in addition to robust learning & career advancement opportunities. GitLab is considered a best place to work for our high performing team members as measured by its talent brand and internal team member satisfaction ( >90%).

1. **High performance culture:** Build on our strength of living our CREDIT values everyday and drive intentional career advancement of underrepresented groups through focused talent development programs. Have top talent team-member satisfaction above 90% with 90% of top talent answering, "My team is a highly performing team," in the affirmative.
1. **Build a diverse team** to drive extraordinary outcomes with 40%+ of team members identifying as non-male and 50% of all Senior Leadership and Executive hires to identify as non-male
1. **All remote:** Hire, retain, and develop the best talent across the world to build a diverse company, while continuing to innovate as thought leaders in recruiting and operating as an efficient, scalable all-remote company.  
90% of the candidates added to our outbound funnel have indicators that suggest they will add to our diversity in some way.
Host the deepest catalog of remote work content of any company in the world.
1. **Best in class learning experience:** Remote L&D influencer through applying GitLab best practices to enable asynchronous/personalized learning and development programs for our team members and the wider community. Invest in learning programs for managers with 90% of team members rate their managers as effective.

As we execute on our strategy, it is important to use our [financial targets](/handbook/being-a-public-company/#long-term-targets-underpin-efficient-durable-growth) as guide rails and mature our internal processes for building a durable business for the long-term.  

More detail on our product strategy can be found on our [direction page](/direction/#vision).

## Principles

1. Fast follower: we don't have to be first to market and we don't suffer from Not Invented Here (NIH)
1. Values: make decisions based on [our values](/handbook/values/), even if it is inconvenient.
1. Reach: go for a broad reach, no focus on business verticals or certain programming languages.
1. Seed then nurture: [plant seeds and nurture the product to build out depth](#seed-then-nurture).
1. Speed: ship every change in the next release to maximize responsiveness and learning.
1. Life balance: we want people to stay with us for a long time, so it is important to [take time off](/handbook/paid-time-off/) and work on life balance; being [all-remote](/company/culture/#all-remote-work) is a large part of the solution.
1. Open Source Stewardship: be a good [steward of GitLab](/company/stewardship) and [collaborate with the wider community](/community/contribute/) to [improve the product together](/handbook/engineering/development/performance-indicators/#mr-rate).

### Seed then nurture

Our GitLab product [vision](/direction/#vision) is to deliver a single application for the entire DevOps lifecycle.  This is a very expansive product vision that requires us to build an enormous amount of product surface area.  Because we are capital constrained, we need to build GitLab as a community. When we are early in a particular area of the product, we will plant seeds by shipping a small [MVC](/handbook/values/#minimal-viable-change-mvc). Shipping functionality that is incomplete to expand the scope sometimes goes against our instincts. However, planting those seeds even in an incomplete state allows others to see our path and contribute. With others contributing, we'll iterate faster and will accelerate the [maturity](/direction/maturity/) of our offering faster than GitLab could on its own.  We can have a **long tail** of categories that are at a minimal [maturity](/direction/maturity/) that don't get investment until they show traction. While these come with a [low level of shame](/handbook/values/#low-level-of-shame) they allow the wider community to contribute and people to express interest. It is much more common for people to contribute to categories that already exist and express interest in categories already shipping in the product. A minimal category is the placeholder to channel energy, and it is our responsibility to till the earth with minimal iterations.

GitLab the product should eventually have depth in every category it offers.  To build out real depth requires a shift in focus, from planting seeds to nurturing the product area to maturity. We should concentrate our nurture investments in the categories that have demonstrated [monthly active usage](/handbook/product/performance-indicators/#structure), revenue contribution, and demonstrated customer demand.  As a product team, we'll be particularly focused on driving monthly active usage at the stage and group level.  This should lead to more [Stages per User](/handbook/product/performance-indicators/#stages-per-user-spu), which is important as each stage added [triples paid conversion](/direction/#strategic-response)!  We'll also be heavily focused on driving usability, by measuring our [system usability score](/handbook/engineering/ux/ux-resources/#system-usability-score), which is a measure of the user perception of GitLab's usability.

If we effectively seed and then nurture, we can fully activate GitLab's [two growth turbos](/company/strategy/#flywheel-with-two-turbos), by creating wider community contributions and driving more stages per user.

## Assumptions

1. [Open source user benefits](http://buytaert.net/acquia-retrospective-2015): significant advantages over proprietary software because of its faster innovation, higher quality, freedom from vendor lock-in, greater security, and lower total cost of ownership.
2. [Open Source stewardship](/company/stewardship/): the wider community comes first, we [play well with others](/handbook/product/gitlab-the-product/#plays-well-with-others) and share the pie with other organizations commercializing GitLab.
3. [Innersourcing](/blog/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/) is needed and companies will choose one solution top-down.
4. Git will be the prevailing technology in the version control market in CY2020.
5. A single application where [interdependence creates exceptional value](https://medium.com/@gerstenzang/developer-tools-why-it-s-hard-to-build-a-big-business-423436993f1c#.ie38a0cls) is superior to a collection of tools or a network of tools. Even so, good integrations are important for network effects and making it possible to integrate GitLab into an organization.
6. To be sustainable we need an open core model that includes a proprietary GitLab EE.

## Pricing

Please see our [pricing model](/handbook/ceo/pricing/) for details


## Dual flywheels

GitLab has two flywheel strategies that reinforce each other: our open core flywheel and our development spend flywheel.
A flywheel strategy is [defined as](https://medium.com/evergreen-business-weekly/flywheel-effect-why-positive-feedback-loops-are-a-meta-competitive-advantage-6d0ed55b67c5) one that has positive feedback loops that build momentum, increasing the payoff of incremental effort.
You can visualize how the flywheels work in congruence via the diagram below. The KPI and responsibilities table lists the relevant indicator and department for every part of the flywheel.

In the open core flywheel, more features drive more users which in turn drive more revenue and more contributions which lead to more users.

```mermaid
graph BT;
  id1(More Users)-->id2(More Revenue);
  id2(More Revenue)-->id3(More Features);
  id3(More Features)-.->id1(MoreUsers);
  id1(More Users)-.->id4(More Contributions);
  id3(More Features)-->id1(More Users);
  id4(More Contributions)-.->id3(More Features);
```
### KPIs and Responsible departments

| Part of flywheel | Key Performance Indicator (KPI) | Department |
|-------------- ---|---------------------------------|------------|
| More Users | [Stage Monthly Active Users](/handbook/product/metrics/#stage-monthly-active-user) | Product |
| More Contributions | [Wider community contributions per release](/handbook/marketing/community-relations/code-contributor-program/#wider-community-contributions-per-milestone) | Community Relations |
| More Features | [Merge Requests per release per engineer in product development](/handbook/engineering/development/performance-indicators/#average-mrs-development-engineers-month) | Engineering and Product Management |
| More Revenue | [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan | Sales |


### Flywheel with two turbos

GitLab is a [complete DevOps platform](/stages-devops-lifecycle/), delivered as a [single application](/handbook/product/single-application/), with [contributions from the wider community](/community/contribute/).

Compared to other DevOps platforms, GitLab leverages one or two unique turbos that boost the company:

1. [Advantages of a single application](/handbook/product/single-application/), leading to more [Stages per User](/handbook/product/metrics/#stages-per-user-spu)
1. [Open Source stewardship](/company/stewardship/), leading to [Wider community contributions](/community/contribute/)

The advantage of a single application manifests itself if people start using more stages of the application.
In the graph below this is visualized with [Stages per User (SpU), knowing that a user using an extra stage triples conversion](/direction/#strategic-response).
Increasing SpU drives both more seats and higher revenue per seat. 

In the development spend flywheel, we capture the relationship between merge requests (MRs), changes in ARR from one period to the next (Delta ARR), hyper growth R&D spend and the resulting impact on MRs. We see that more MRs increase stage maturity which drives more monthly active users and stages per user which in turn drives more seats and more revenue which funds R&D spend and leads to more MRs.

```mermaid
graph TB
  id1(Wider community contributions Turbo) --> id2
  id2(Engineering MR Rate) --> id3
  id3(Stage maturity increase) --> id4
  id3 --> id9
  id4(MAU increase) --> id5
  id5(More Licensed users) --> id6
  id6(IACV) --> id12
  id12(ARR) --> id8
  id7(R&D spend ratio) --> id8
  id8(R&D investment) --> id11
  id11(Team MR rate) --> id2
  id9(SpU increase Turbo) --> id5
  id9 --> id10
  id10(Higher Revenue per licensed user) --> id6
  id6 --> id7
```

Legend with links to the relevant metrics:

1. [Wider community contributions Turbo](/handbook/marketing/community-relations/code-contributor-program/#wider-community-contributions-per-milestone)
1. [Engineering MR rate](/handbook/engineering/development/performance-indicators/#mr-rate)
1. [Stage maturity](/direction/maturity/) increase
1. [MAU](/handbook/product/metrics/#monthly-active-users-mau) increase and note that MAU * SpU = [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau)
1. [SpU](/handbook/product/metrics/#stages-per-user-spu) increase Turbo which is only possible in a [single application with multiple stages](/handbook/product/single-application/)
1. More [Licensed users](/handbook/sales/#licensed-users) is due to an increase in unlicensed users (MAU) and the increase in [SpU](/handbook/product/metrics/#stages-per-user-spu) leading to a higher conversion from free to paid and a higher gross retention.
1. Higher [Revenue per licensed user](/handbook/sales/#revenue-per-licensed-user-also-known-as-arpu) due to the [Advantages of a single application](/handbook/product/single-application/)
1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) is our [most important KPI](/handbook/business-ops/data-team/kpi-index/#gitlab-kpis)
1. [ARR](/handbook/sales/#annual-recurring-revenue-arr) stands for Annual Recurring Revenue and IACV increases it.
1. R&D spend ratio becomes higher if the growth rate is higher due to the [Hypergrowth Rule](/handbook/finance/financial-planning-and-analysis/hypergrowth-rule/)
1. R&D investment is the amount of money spend on Product Management and Engineering excluding Support
1. Team MR rate: can't find right now, also we need to change Wider community contributions from number to the ratio when we have that.

## Publicly viewable OKRs and KPIs

To make sure our goals are clearly defined and aligned throughout the organization, we make use of [Objectives and Key Results (OKRs)](/company/okrs/) and [Key Performance Indicators (KPIs)](/handbook/ceo/kpis/) which are both publicly viewable.

## Plan

Our near terms plans are captured on our [direction page](/direction).

## Why is this page public?

Our strategy is completely public because transparency is one of our [values](/handbook/values/).
We're not afraid of sharing our strategy because, as Peter Drucker said,
"Strategy is a commodity, execution is an art."
