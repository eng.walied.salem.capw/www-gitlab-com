---
layout: markdown_page
title: "FY22-Q1 OKRs"
description: "View GitLabs Objective-Key Results for FY22 Q1. Learn more here!"
canonical_path: "/company/okrs/fy22-q1/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2021 to April 30,2021.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule
The by-the-book schedule for the OKR timeline would be

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2020-12-28 | CEO shares top goals with E-group for feedback |
| -4 | 2021-01-04 | CEO pushes top goals to this page |
| -3 | 2021-01-11 | [E-group propose OKRs for their functions in Epics and Issues in #okrs](https://about.gitlab.com/company/okrs/#executives-propose-okrs-for-their-functions) |
| -2 | 2021-01-18 | E-group 50 minute draft review meeting |
| -2 | 2021-01-18 | E-group discusses with their respective teams and finalize their OKRs |
| -1 | 2021-01-25 | CEO reports post final MRs in #okrs slack channel and @ mention the CEO and CoS for approval |
| 0  | 2021-02-01 | CoS updates OKR page for current quarter to be active |


## OKRs

### 1. CEO: Grow Incremental ACV (IACV)

### 2. CEO: Popular next generation product

### 3. CEO: Great team
